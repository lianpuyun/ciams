<?php

return [

    [
        'name' => '控制台',
        'uri' => '',
        'skip' => 0,
        'child' => [
            [
                'name' => '后台',
                'uri' => 'admin/home/main',
                'skip' => 0,
                'child' => [
                    [
                        'name' => '后台首页',
                        'uri' => 'admin/home/index',
                        'skip' => 1,
                    ],
                    [
                        'name' => '后台主页',
                        'uri' => 'admin/home/main',
                        'skip' => 1,
                    ],

                ],
            ],
            [
                'name' => '系统',
                'uri' => '',
                'child' => [
                    [
                        'name' => '系统设置',
                        'uri' => 'admin/system/index',
                        'skip' => 0,
                    ],
                    [
                        'name' => '操作日志',
                        'uri' => 'admin/system/oplog',
                        'skip' => 0,
                    ],
                    [
                        'name' => '错误日志',
                        'uri' => 'admin/system/debug',
                        'skip' => 0,
                    ],

                ],
            ],
            [
                'name' => '模块管理',
                'uri' => '',
                'child' => [
                    [
                        'name' => '模块列表',
                        'uri' => 'admin/module/config',
                        'skip' => 0,
                    ],
                    [
                        'name' => '安装模块',
                        'uri' => 'admin/module/instal',
                        'skip' => 0,
                    ],
                    [
                        'name' => '卸载模块',
                        'uri' => 'admin/module/uninstall',
                        'skip' => 0,
                    ],
                ],
            ],
        ],
    ],

    [
        'name' => '内容',
        'uri' => '',
        'skip' => 0,
        'child' => [

            [
                'name' => '文章',
                'uri' => '',
                'child' => [
                    [
                        'name' => '文章列表',
                        'uri' => 'admin/article/index',
                        'skip' => 0,
                    ],
                    [
                        'name' => '添加文章',
                        'uri' => 'admin/article/add',
                        'skip' => 0,
                    ],
                    [
                        'name' => '修改文章',
                        'uri' => 'admin/article/edit',
                        'skip' => 0,
                    ],
                    [
                        'name' => '删除文章',
                        'uri' => 'admin/article/del',
                        'skip' => 0,
                    ],
                    [
                        'name' => '文章分类列表',
                        'uri' => 'admin/article/catindex',
                        'skip' => 0,
                    ],
                    [
                        'name' => '添加文章分类',
                        'uri' => 'admin/article/catadd',
                        'skip' => 0,
                    ],
                    [
                        'name' => '修改文章分类',
                        'uri' => 'admin/article/catedit',
                        'skip' => 0,
                    ],
                    [
                        'name' => '删除文章分类',
                        'uri' => 'admin/article/catdel',
                        'skip' => 0,
                    ],
                ],
            ],
            [
                'name' => '导航链接',
                'uri' => '',
                'child' => [
                    [
                        'name' => '导航列表',
                        'uri' => 'admin/navbar/index',
                        'skip' => 0,
                    ],
                    [
                        'name' => '添加导航',
                        'uri' => 'admin/navbar/add',
                        'skip' => 0,
                    ],
                    [
                        'name' => '修改导航',
                        'uri' => 'admin/navbar/edit',
                        'skip' => 0,
                    ],
                    [
                        'name' => '删除导航',
                        'uri' => 'admin/navbar/del',
                        'skip' => 0,
                    ],
                    [
                        'name' => '分类列表',
                        'uri' => 'admin/navbar/catindex',
                        'skip' => 0,
                    ],
                    [
                        'name' => '添加分类',
                        'uri' => 'admin/navbar/catadd',
                        'skip' => 0,
                    ],
                    [
                        'name' => '修改分类',
                        'uri' => 'admin/navbar/catedit',
                        'skip' => 0,
                    ],
                    [
                        'name' => '删除分类',
                        'uri' => 'admin/navbar/catdel',
                        'skip' => 0,
                    ],
                ],
            ],
            [
                'name' => '单页',
                'uri' => '',
                'child' => [
                    [
                        'name' => '单页列表',
                        'uri' => 'admin/pages/index',
                        'skip' => 0,
                    ],
                    [
                        'name' => '添加单页',
                        'uri' => 'admin/pages/add',
                        'skip' => 0,
                    ],
                    [
                        'name' => '修改单页',
                        'uri' => 'admin/pages/edit',
                        'skip' => 0,
                    ],
                    [
                        'name' => '删除单页',
                        'uri' => 'admin/pages/del',
                        'skip' => 0,
                    ],
                ],
            ],
            [
                'name' => '广告',
                'uri' => '',
                'child' => [
                    [
                        'name' => '广告列表',
                        'uri' => 'admin/blocks/index',
                        'skip' => 0,
                    ],
                    [
                        'name' => '添加广告',
                        'uri' => 'admin/blocks/add',
                        'skip' => 0,
                    ],
                    [
                        'name' => '修改广告',
                        'uri' => 'admin/blocks/edit',
                        'skip' => 0,
                    ],
                    [
                        'name' => '删除广告',
                        'uri' => 'admin/blocks/del',
                        'skip' => 0,
                    ],
                    [
                        'name' => '分类列表',
                        'uri' => 'admin/blocks/catindex',
                        'skip' => 0,
                    ],
                    [
                        'name' => '添加分类',
                        'uri' => 'admin/blocks/catadd',
                        'skip' => 0,
                    ],
                    [
                        'name' => '修改分类',
                        'uri' => 'admin/blocks/catedit',
                        'skip' => 0,
                    ],
                    [
                        'name' => '删除分类',
                        'uri' => 'admin/blocks/catdel',
                        'skip' => 0,
                    ],
                ],
            ],
            [
                'name' => '碎片文件',
                'uri' => '',
                'child' => [
                    [
                        'name' => '碎片列表',
                        'uri' => 'admin/pieces/index',
                        'skip' => 0,
                    ],
                    [
                        'name' => '添加碎片',
                        'uri' => 'admin/pieces/add',
                        'skip' => 0,
                    ],
                    [
                        'name' => '修改碎片',
                        'uri' => 'admin/pieces/edit',
                        'skip' => 0,
                    ],
                    [
                        'name' => '删除碎片',
                        'uri' => 'admin/pieces/del',
                        'skip' => 0,
                    ],
                ],
            ],
        ],
    ],

    [
        'name' => '权限',
        'uri' => '',
        'skip' => 0,
        'child' => [
            [
                'name' => '菜单管理',
                'uri' => '',
                'child' => [
                    [
                        'name' => '菜单列表',
                        'uri' => 'admin/menu/index',
                        'skip' => 0,
                    ],
                    [
                        'name' => '添加菜单',
                        'uri' => 'admin/menu/add',
                        'skip' => 0,
                    ],
                    [
                        'name' => '修改菜单',
                        'uri' => 'admin/menu/edit',
                        'skip' => 0,
                    ],
                    [
                        'name' => '删除菜单',
                        'uri' => 'admin/menu/del',
                        'skip' => 0,
                    ],
                ],
            ],
            [
                'name' => '管理员',
                'uri' => '',
                'child' => [
                    [
                        'name' => '管理员列表',
                        'uri' => 'admin/rbac/index',
                        'skip' => 0,
                    ],
                    [
                        'name' => '添加管理员',
                        'uri' => 'admin/rbac/add',
                        'skip' => 0,
                    ],
                    [
                        'name' => '修改管理员',
                        'uri' => 'admin/rbac/edit',
                        'skip' => 0,
                    ],
                    [
                        'name' => '删除管理员',
                        'uri' => 'admin/rbac/del',
                        'skip' => 0,
                    ],
                ],
            ],
            [
                'name' => '角色管理',
                'uri' => '',
                'child' => [
                    [
                        'name' => '角色列表',
                        'uri' => 'admin/rbac/role',
                        'skip' => 0,
                    ],
                    [
                        'name' => '添加角色',
                        'uri' => 'admin/rbac/addRole',
                        'skip' => 0,
                    ],
                    [
                        'name' => '修改角色',
                        'uri' => 'admin/rbac/editRole',
                        'skip' => 0,
                    ],
                    [
                        'name' => '删除角色',
                        'uri' => 'admin/rbac/delRole',
                        'skip' => 0,
                    ],
                ],
            ],
            [
                'name' => '权限规则',
                'uri' => '',
                'child' => [
                    [
                        'name' => '规则列表',
                        'uri' => 'admin/rbac/rule',
                        'skip' => 0,
                    ],
                    [
                        'name' => '添加规则',
                        'uri' => 'admin/rbac/addRule',
                        'skip' => 0,
                    ],
                    [
                        'name' => '修改规则',
                        'uri' => 'admin/rbac/editRule',
                        'skip' => 0,
                    ],
                    [
                        'name' => '删除规则',
                        'uri' => 'admin/rbac/delRule',
                        'skip' => 0,
                    ],
                ],
            ],
        ],
    ],

    [
        'name' => '用户',
        'uri' => '',
        'skip' => 0,
        'child' => [
            [
                'name' => '用户管理',
                'uri' => '',
                'child' => [
                    [
                        'name' => '用户列表',
                        'uri' => 'admin/user/index',
                        'skip' => 0,
                    ],
                    [
                        'name' => '添加用户',
                        'uri' => 'admin/user/add',
                        'skip' => 0,
                    ],
                    [
                        'name' => '修改用户',
                        'uri' => 'admin/user/edit',
                        'skip' => 0,
                    ],
                    [
                        'name' => '删除用户',
                        'uri' => 'admin/user/del',
                        'skip' => 0,
                    ],
                ],
            ],
            [
                'name' => '用户分组',
                'uri' => '',
                'skip' => 0,
                'child' => [
                    [
                        'name' => '分组管理',
                        'uri' => 'admin/user/group',
                        'skip' => 0,
                    ],
                    [
                        'name' => '添加分组',
                        'uri' => 'admin/user/addGroup',
                        'skip' => 0,
                    ],
                    [
                        'name' => '修改分组',
                        'uri' => 'admin/user/editGroup',
                        'skip' => 0,
                    ],
                    [
                        'name' => '删除分组',
                        'uri' => 'admin/user/delGroup',
                        'skip' => 0,
                    ],
                ],
            ],
        ],
    ],

];
