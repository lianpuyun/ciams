SET FOREIGN_KEY_CHECKS=0;

DROP TABLE IF EXISTS `{dbprefix}admin`;
CREATE TABLE `{dbprefix}admin` (
  `userid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` char(40) NOT NULL DEFAULT '' COMMENT '邮箱地址',
  `username` varchar(50) NOT NULL DEFAULT '' COMMENT '用户名',
  `password` char(32) NOT NULL DEFAULT '' COMMENT '加密密码',
  `salt` char(10) NOT NULL COMMENT '随机加密码',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '姓名',
  `phone` char(20) NOT NULL DEFAULT '' COMMENT '手机号码',
  `avatar` varchar(255) NOT NULL DEFAULT '' COMMENT '头像地址',
  `roleid` smallint(5) unsigned NOT NULL DEFAULT 0 COMMENT '管理组idroleid',
  `regip` varchar(15) NOT NULL DEFAULT '' COMMENT '注册ip',
  `regtime` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '注册时间',
  `status` tinyint(1) unsigned NOT NULL DEFAULT 0 COMMENT '0禁用1正常',
  `islock` tinyint(1) unsigned NOT NULL DEFAULT 0 COMMENT '0正常1登录错误被锁定',
  `unlock` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '解锁时间',
  PRIMARY KEY (`userid`),
  KEY `username` (`username`),
  KEY `email` (`email`),
  KEY `roleid` (`roleid`),
  KEY `phone` (`phone`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='管理员表';


DROP TABLE IF EXISTS `{dbprefix}admin_login`;
CREATE TABLE `{dbprefix}admin_login` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '管理员userid',
  `oauthid` varchar(30) NOT NULL COMMENT '快捷登录方式',
  `loginip` varchar(50) NOT NULL COMMENT '登录Ip',
  `platform` varchar(50) NOT NULL COMMENT '登录平台 mobile pc small weixin...',
  `logintime` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '登录时间',
  `useragent` varchar(255) NOT NULL COMMENT '客户端信息',
  PRIMARY KEY (`id`),
  KEY `userid` (`userid`),
  KEY `loginip` (`loginip`),
  KEY `logintime` (`logintime`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='管理员登录日志';

DROP TABLE IF EXISTS `{dbprefix}admin_menu`;
CREATE TABLE `{dbprefix}admin_menu` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `pid` smallint(5) unsigned NOT NULL COMMENT '上级菜单id',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '菜单语言名称',
  `uri` varchar(255) NOT NULL DEFAULT '' COMMENT 'uri字符串',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '外链地址',
  `mark` varchar(100) NOT NULL DEFAULT '' COMMENT '菜单标识',
  `show` tinyint(1) unsigned NOT NULL DEFAULT 0 COMMENT '是否隐藏',
  `displayorder` tinyint(3) unsigned NOT NULL DEFAULT 0 COMMENT '排序值',
  `icon` varchar(100) NOT NULL DEFAULT '' COMMENT '图标标示',
  `param` varchar(200) NOT NULL DEFAULT '',
  `childids` varchar(255) NOT NULL DEFAULT '',
  `pids` varchar(255) NOT NULL DEFAULT '',
  `child` tinyint(1) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `list` (`pid`),
  KEY `displayorder` (`displayorder`),
  KEY `mark` (`mark`),
  KEY `show` (`show`),
  KEY `uri` (`uri`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='后台菜单表';

DROP TABLE IF EXISTS `{dbprefix}admin_oplog`;
CREATE TABLE `{dbprefix}admin_oplog` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '用户userid',
  `ip` varchar(100) NOT NULL DEFAULT '' COMMENT '操作IP地址',
  `inputtime` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '操作时间',
  `action` varchar(255) NOT NULL DEFAULT '' COMMENT '操作描述',
  `year` char(4) NOT NULL DEFAULT '' COMMENT '2019',
  `month` char(7) NOT NULL DEFAULT '' COMMENT '年月2019-05',
  `day` char(10) NOT NULL DEFAULT '' COMMENT '年月日2019-02-05',
  `username` varchar(50) NOT NULL DEFAULT '' COMMENT '用户名',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '链接',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `{dbprefix}attachment`;
CREATE TABLE `{dbprefix}attachment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(10) NOT NULL DEFAULT 0 COMMENT '会员id',
  `author` varchar(50) NOT NULL DEFAULT '' COMMENT '会员',
  `related` varchar(50) NOT NULL DEFAULT '' COMMENT '相关表标识table-id',
  `download` mediumint(8) unsigned NOT NULL DEFAULT 0 COMMENT '下载次数',
  `filesize` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '文件大小',
  `fileext` varchar(20) NOT NULL DEFAULT '' COMMENT '文件扩展名',
  `filemd5` varchar(50) NOT NULL DEFAULT '' COMMENT '文件md5值',
  `adminid` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '管理员id',
  `filename` varchar(255) NOT NULL DEFAULT '' COMMENT '名称',
  `attachment` varchar(255) NOT NULL DEFAULT '',
  `inputtime` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '上传时间',
  `fileinfo` varchar(255) NOT NULL DEFAULT '' COMMENT '文件信息',
  `filename2` varchar(255) NOT NULL DEFAULT '' COMMENT '新名称',
  PRIMARY KEY (`id`),
  KEY `userid` (`userid`),
  KEY `author` (`author`),
  KEY `relatedtid` (`related`),
  KEY `fileext` (`fileext`),
  KEY `filemd5` (`filemd5`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='附件表';

DROP TABLE IF EXISTS `{dbprefix}black`;
CREATE TABLE `{dbprefix}black` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增',
  `ip` varchar(50) NOT NULL DEFAULT '' COMMENT 'ip',
  `intip` int(10) unsigned NOT NULL DEFAULT 0 COMMENT 'ip2long',
  `type` tinyint(1) unsigned NOT NULL DEFAULT 0 COMMENT '0单个ip，1是IP段 2是匹配*',
  `inputtime` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '添加时间',
  `endtime` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '有效时间0表示长期',
  `status` tinyint(1) unsigned NOT NULL DEFAULT 1 COMMENT '1拦截中 0不拦截',
  `startip` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '开始ip',
  `endip` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '结束ip',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `{dbprefix}navbar`;
CREATE TABLE `{dbprefix}navbar` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `catid` smallint(5) unsigned NOT NULL DEFAULT 0,
  `name` varchar(50) NOT NULL DEFAULT '',
  `url` varchar(255) NOT NULL DEFAULT '',
  `thumb` varchar(255) NOT NULL DEFAULT '',
  `displayorder` smallint(5) unsigned NOT NULL DEFAULT 0,
  `show` tinyint(1) unsigned NOT NULL DEFAULT 0 COMMENT '0不显示1显示',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT 'alt title',
  `enname` varchar(255) NOT NULL DEFAULT '' COMMENT '英文名称',
  `pid` smallint(5) unsigned NOT NULL DEFAULT 0,
  `childids` varchar(255) NOT NULL DEFAULT '',
  `pids` varchar(255) NOT NULL DEFAULT '',
  `child` tinyint(1) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `{dbprefix}navbar_cat`;
CREATE TABLE `{dbprefix}navbar_cat` (
  `id` smallint(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL DEFAULT '' COMMENT '分类名称',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `{dbprefix}pages`;
CREATE TABLE `{dbprefix}pages` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `pid` smallint(5) unsigned NOT NULL DEFAULT 0 COMMENT '上级id',
  `pids` varchar(255) NOT NULL COMMENT '所有上级id',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '单页名称',
  `dirname` varchar(30) NOT NULL DEFAULT '' COMMENT '栏目目录',
  `pdirname` varchar(100) NOT NULL COMMENT '上级目录',
  `child` tinyint(1) unsigned NOT NULL COMMENT '是否有子类',
  `childids` varchar(255) NOT NULL COMMENT '下级所有id',
  `thumb` varchar(255) NOT NULL COMMENT '缩略图',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT 'seo标题',
  `keywords` varchar(255) NOT NULL COMMENT 'seo关键字',
  `description` varchar(255) NOT NULL COMMENT 'seo描述',
  `content` mediumtext DEFAULT NULL COMMENT '单页内容',
  `template` varchar(30) NOT NULL COMMENT '模板文件',
  `urllink` varchar(255) NOT NULL DEFAULT '' COMMENT 'url外链',
  `getchild` tinyint(1) unsigned NOT NULL COMMENT '将下级第一个菜单作为当前菜单',
  `show` tinyint(1) unsigned NOT NULL DEFAULT 1 COMMENT '是否显示',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT 'url地址',
  `displayorder` tinyint(5) unsigned NOT NULL DEFAULT 0 COMMENT '排序',
  `enname` varchar(50) NOT NULL DEFAULT '' COMMENT '英文名称',
  `inputtime` int(10) unsigned NOT NULL DEFAULT 0,
  `updatetime` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '更新时间',
  `status` tinyint(1) unsigned NOT NULL DEFAULT 0 COMMENT '0,1',
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`),
  KEY `show` (`show`),
  KEY `displayorder` (`displayorder`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='单页表';

DROP TABLE IF EXISTS `{dbprefix}pages_data`;
CREATE TABLE `{dbprefix}pages_data` (
  `id` int(10) unsigned NOT NULL COMMENT 'pages表id',
  `attachment` varchar(255) NOT NULL DEFAULT '' COMMENT '附件'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `{dbprefix}rbac_auth`;
CREATE TABLE `{dbprefix}rbac_auth` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT '权限名称',
  `uri` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT '权限路径',
  `skip` tinyint(1) unsigned NOT NULL DEFAULT 0 COMMENT '0验证1忽略',
  `pid` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '上级',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `{dbprefix}rbac_role`;
CREATE TABLE `{dbprefix}rbac_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '描述说明',
  `status` tinyint(1) unsigned NOT NULL DEFAULT 0 COMMENT '0关闭1启用',
  `auth` text NOT NULL COMMENT '权限',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
INSERT INTO `{dbprefix}rbac_role` VALUES ('1', '创始人', '', '1', '');

DROP TABLE IF EXISTS `{dbprefix}rbac_rule`;
CREATE TABLE `{dbprefix}rbac_rule` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '权限名称',
  `uri` varchar(160) NOT NULL DEFAULT '' COMMENT '权限路径',
  `skip` tinyint(1) unsigned NOT NULL DEFAULT 0 COMMENT '0验证1忽略',
  `pid` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '上级',
  `level` tinyint(3) unsigned NOT NULL DEFAULT 1 COMMENT '最多3,1-2-3',
  `mark` varchar(16) NOT NULL DEFAULT '' COMMENT '标识',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `{dbprefix}setting`;
CREATE TABLE `{dbprefix}setting` (
  `param` varchar(255) NOT NULL COMMENT '参数名称英文',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '标题',
  `value` text NOT NULL COMMENT '3000字',
  `updatetime` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`param`),
  UNIQUE KEY `param` (`param`) USING BTREE,
  KEY `value` (`value`(333)) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `ams_setting` VALUES ('site', '', '{\"SITE_NAME\":\"CIAMS\",\"SITE_DOMAIN\":\"www.ciams.com\",\"SITE_DOMAINS\":\"\",\"SITE_HTML_DIR\":\"\",\"SITE_MOBILE\":\"\",\"SITE_CLOSE\":\"0\",\"SITE_CLOSE_MSG\":\"\\u7f51\\u7ad9\\u5173\\u95ed\\u65f6\\u7684\\u663e\\u793a\\u4fe1\\u606f\",\"SITE_LANGUAGE\":\"en\",\"SITE_THEME\":\"default\",\"SITE_THEME_URL\":\"\",\"SITE_TEMPLATE\":\"default\",\"SITE_TITLE\":\"\\u7f51\\u7ad9\\u9996\\u9875SEO\\u6807\\u9898-\\u6211\\u7684\\u6807\\u9898\",\"SITE_SEOJOIN\":\"_\",\"SITE_KEYWORDS\":\"\\u7f51\\u7ad9SEO\\u5173\\u952e\\u5b57\",\"SITE_DESCRIPTION\":\"\\u7f51\\u7ad9SEO\\u63cf\\u8ff0\\u4fe1\\u606f\",\"SITE_IMAGE_VRTALIGN\":\"top\",\"SITE_MOBILE_OPEN\":\"1\",\"SITE_IMAGE_HORALIGN\":\"left\",\"SITE_HTTPS\":\"0\",\"SITE_IMAGE_CONTENT\":\"1\",\"SITE_IMAGE_OVERLAY\":\"default.png\",\"SITE_EMAIL\":\"976510651@qq.com\",\"SITE_ATTACHMENT_URL\":\"\",\"SITE_KEY\":\"MOK420BADBA5712DD67\",\"SITE_ATTACHMENT_PATH\":\"\",\"SITE_IMAGE_FONT\":\"default.ttf\",\"SITE_IMAGE_RATIO\":\"1\",\"SITE_TIMEZONE\":\"8\",\"SITE_ADMIN_PAGESIZE\":\"10\",\"SITE_IMAGE_WATERMARK\":\"0\",\"SITE_IMAGE_VRTOFFSET\":\"\",\"SITE_IMAGE_HOROFFSET\":\"\",\"SITE_IMAGE_TYPE\":\"0\",\"SITE_IMAGE_OPACITY\":\"\",\"SITE_IMAGE_SIZE\":\"16\",\"SITE_IMAGE_TEXT\":\"\",\"SITE_RECEIVER\":\"\",\"SITE_TEMPALTE\":\"default\",\"SITE_VISITS\":\"0\",\"open\":\"0\",\"SITE_ADMIN_LOCK\":\"3\",\"SITE_ADMIN_LOCK_HOUR\":\"5\"}', '1602472727');

DROP TABLE IF EXISTS `{dbprefix}black`;
CREATE TABLE `{dbprefix}black` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增',
  `ip` varchar(50) NOT NULL DEFAULT '' COMMENT 'ip',
  `intip` int(10) unsigned NOT NULL DEFAULT 0 COMMENT 'ip2long',
  `type` tinyint(1) unsigned NOT NULL DEFAULT 0 COMMENT '0单个ip，1是IP段 2是匹配*',
  `inputtime` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '添加时间',
  `endtime` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '有效时间0表示长期',
  `status` tinyint(1) unsigned NOT NULL DEFAULT 1 COMMENT '1拦截中 0不拦截',
  `startip` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '开始ip',
  `endip` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '结束ip',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `{dbprefix}module`;
CREATE TABLE `{dbprefix}module` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(160) NOT NULL DEFAULT '' COMMENT '模块名称',
  `dirname` varchar(50) NOT NULL DEFAULT '' COMMENT '文件夹名称',
  `status` tinyint(1) unsigned NOT NULL DEFAULT 0 COMMENT '0关闭1正常',
  `config` text DEFAULT NULL COMMENT '设置',
  `addtime` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '安装时间',
  `version` varchar(20) NOT NULL DEFAULT '' COMMENT '版本',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `{dbprefix}pieces`;
CREATE TABLE `{dbprefix}pieces` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '标题',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '参数名称英文',
  `content` text NOT NULL COMMENT '3000字',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '描述简介',
  `type` varchar(20) NOT NULL DEFAULT '' COMMENT '字段类型',
  `issystem` tinyint(1) unsigned NOT NULL DEFAULT 0 COMMENT '是否是系统参数',
  `displayorder` tinyint(1) unsigned NOT NULL DEFAULT 0 COMMENT '0排序',
  `updatetime` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;