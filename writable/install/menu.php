<?php

return [

    'admin' => [
        [
            'name' => '控制台',
            'mark' => 'home',
            'icon' => 'layui-icon layui-icon-console',
            'child' => [
                [
                    'name' => '后台首页',
                    'uri' => 'admin/home/main',
                    'icon' => 'layui-icon layui-icon-home',
                ],
                [
                    'name' => '系统设置',
                    'uri' => 'admin/system/index',
                    'icon' => 'layui-icon layui-icon-set-sm',
                ],
                [
                    'name' => '模块管理',
                    'uri' => 'admin/module/index',
                    'icon' => 'layui-icon layui-icon-android',
                ],
                [
                    'name' => '操作日志',
                    'uri' => 'admin/system/oplog',
                    'icon' => 'layui-icon layui-icon-log',
                ],
                [
                    'name' => '错误日志',
                    'uri' => 'admin/system/debug',
                    'icon' => 'layui-icon layui-icon-log',
                ],
                [
                    'name' => '碎片管理',
                    'uri' => 'admin/pieces/index',
                    'icon' => 'layui-icon layui-icon-app',
                ],
            ],
        ],
        [
            'name' => '后台权限',
            'mark' => 'rbac',
            'icon' => 'layui-icon layui-icon-auz',
            'child' => [
                [
                    'name' => '菜单管理',
                    'uri' => 'admin/menu/index',
                    'icon' => 'layui-icon layui-icon-spread-left',
                ],
                [
                    'name' => '管理员',
                    'uri' => 'admin/rbac/index',
                    'icon' => 'layui-icon layui-icon-user',
                ],
                [
                    'name' => '角色管理',
                    'uri' => 'admin/rbac/role',
                    'icon' => 'layui-icon layui-icon-friends',
                ],
                [
                    'name' => '权限规则',
                    'uri' => 'admin/rbac/rule',
                    'icon' => 'layui-icon layui-icon-auz',
                ],
            ],
        ],
    ],
];
