<?php
namespace Apps\News\Controllers\Admin;

use Apps\News\Models\NewsModel;
use App\Controllers\AdminBase;

class Home extends AdminBase
{

    protected $newsModel;
    public $cats;

    public function __construct(...$param)
    {
        $this->newsModel = new NewsModel();
        $this->cats = $this->newsModel->getCatTreeData();
    }

    //管理列表
    public function index()
    {
        if (IS_POST) {
            $page = $this->request->getPost('page');
            $limit = $this->request->getPost('limit');
            list($list, $total, $param) = $this->newsModel->limit_page($page, $limit, 0);
            if ($list) {
                foreach ($list as $k => $v) {
                    $list[$k]['catname'] = $v['catid'] ? $this->cats[$v['catid']]['name'] : '';
                    $list[$k]['inputtime_data'] = date('Y-m-d H:i:s', $v['inputtime']);
                    $list[$k]['updatetime_data'] = date('Y-m-d H:i:s', $v['updatetime']);
                }
            }
            $return = [
                'total' => $total,
                'data' => $list,
            ];
            $this->_json(0, 'ok', $return);
        }
        $vdata = [
            'cats' => $this->cats,
        ];
        return view('news/index.html', $vdata);
    }

    function list() {
        if (IS_POST) {
            $page = $this->request->getPost('page');
            $limit = $this->request->getPost('limit');
            list($list, $total, $param) = $this->newsModel->limit_page($page, $limit, 0);
            if ($list) {
                foreach ($list as $k => $v) {
                    $list[$k]['catname'] = $v['catid'] ? $this->cats[$v['catid']]['name'] : '';
                    $list[$k]['inputtime_data'] = date('Y-m-d H:i:s', $v['inputtime']);
                    $list[$k]['updatetime_data'] = date('Y-m-d H:i:s', $v['updatetime']);
                }
            }
            $return = [
                'total' => $total,
                'data' => $list,
            ];
            $this->_json(0, 'ok', $return);
        }
        $vdata = [
            'cats' => $this->cats,
        ];
        return view('news/index.html', $vdata);
    }

    //添加
    public function add()
    {
        if (IS_POST) {
            $data = $this->request->getPost('data');
            $rt = $this->newsModel->add($data);
            if ($rt['code'] == 0) {
                $this->addSystemLog('添加新闻公告: ' . $data['title']);
                $this->_json(0, '操作成功');
            }
            $this->_json(1, $rt['msg']);
        }
        $vdata = [
            'cats' => $this->cats,
        ];
        return view('news/add.html', $vdata);
    }

    //修改
    public function edit()
    {
        $id = intval($this->request->getGet('id'));
        if (IS_POST) {
            $data = $this->request->getPost('data');
            $rt = $this->newsModel->edit($id, $data);
            if ($rt['code'] == 0) {
                $this->addSystemLog('修改新闻公告: ' . $data['title']);
                $this->_json(0, '操作成功');
            }
            $this->_json(1, $rt['msg']);
        }
        $data = $this->newsModel->get($id);
        $vdata = [
            'cats' => $this->cats,
            'data' => $data,
        ];
        return view('news/add.html', $vdata);
    }

    //删除
    public function del()
    {
        $yes = $no = 0;
        if (IS_POST) {
            $ids = $this->request->getPost('ids');
            mok_clean_xss($ids);
            if ($ids) {
                if (is_array($ids)) {
                    foreach ($ids as $id) {
                        $rt = $this->newsModel->del($id);
                        if ($rt['code'] == 0) {
                            $this->addSystemLog('删除新闻:#' . $id);
                            $yes++;
                        } else {
                            $no++;
                        }
                    }
                } else {
                    $rt = $this->newsModel->del($ids);
                    if ($rt['code'] == 0) {
                        $this->addSystemLog('删除新闻:#' . $ids);
                        $yes++;
                    } else {
                        $no++;
                    }
                }

            } else {
                $this->_json(1, '参数错误');
            }
            $this->_json(0, '操作结果 成功：' . $yes . ',失败' . $no);
        }
    }

    //上传文件
    public function upload()
    {
        if (IS_POST || IS_AJAX_POST) {
            $file = $this->request->getFile('file');
            $postdata = $this->request->getPost();

            //ip
            $ip = $this->request->getIPAddress();
            //文件大小
            $size = $file->getSize();
            //文件后缀名没有.
            $ext = $file->getExtension();
            //文件类型
            $type = $file->getType();
            //文件名称
            $name = $file->getName();
            //mime
            $mime = $file->getMimeType();
            //临时文件目录
            $tempfile = $file->getTempName();
            //实际存储目录
            $dir = 'uploads/' . date('Ym') . '/' . date('d') . '/';
            //重命名
            $newName = $file->getRandomName();
            $nfile = $dir . $newName;

            if (!in_array($ext, ['png', 'jpg', 'jpeg', 'gif', 'bmp'])) {
                $this->_json(1, '上传失败', '错误:请上传图片');
            }

            //移动文件
            if ($file->isValid() && !$file->hasMoved()) {
                $file->move(WRITEPATH . $dir, $newName);
            } elseif (!$file->isValid()) {
                if ($file->getErrorString()) {
                    $this->_json(1, '上传失败', '错误:' . $file->getError());
                }
            }

            if ($file->hasMoved()) {
                $info = getimagesize(WRITEPATH . $nfile);
                $thumb = '/assets/mokui/image/ext/' . $ext . '.png';
                if ($ext === 'png' || $ext === 'jpg' || $ext === 'jpeg' || $ext === 'gif') {
                    $thumb = thumb($nfile, 200, 200);
                }
                $data = [
                    'filename' => $name,
                    'fileext' => $ext,
                    'filesize' => $size,
                    'attachment' => $nfile,
                    'inputtime' => time(),
                    'fileinfo' => json_encode($info),
                    'adminid' => 0,
                    'newname' => $newName,
                ];
                $this->AttachmentModel = model('App\Models\AttachmentModel');
                $rt = $this->AttachmentModel->add($data);
                if ($rt) {
                    $rt['data'] = ['thumb' => $thumb, 'src' => SITE_URL . trim($nfile, '/'), 'fileid' => $rt['data'], 'title' => $name, 'size' => $size, 'ext' => $ext];
                }
                $this->_json($rt);
            } else {
                //移动失败
                $data = ['src' => $nfile, 'title' => $name, 'size' => $size, 'ext' => $ext];
                $this->_json(1, '上传失败', $data);
            }
        }
        $this->_json(1, '上传失败:POST');
    }

}
