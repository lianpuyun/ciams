<?php
namespace Apps\News\Controllers;

use Apps\News\Controllers\MyBase;
use Apps\News\Models\NewsModel;

class Home extends MyBase
{
    protected $newsModel;
    public $cats;

    public function __construct()
    {
        $this->newsModel = new NewsModel();
        $this->cats = $this->newsModel->getAllCats();
    }

    public function index($page = 1, $limit = 10)
    {
        $list = $this->newsModel->getList($catid = 0, $page, $limit);
        $vdata = [
            'list' => $list,
            'cats' => $this->cats,
        ];
        return view('index.html', $vdata);
    }

    //分类
    public function cat($catid = 0, $page = 1, $limit = 10)
    {
        $list = $this->newsModel->getList($catid, $page, $limit);
        $vdata = [
            'list' => $list,
            'cats' => $this->cats,
        ];
        return view('list.html', $vdata);
    }

    //详情
    public function show($id)
    {
        $data = $this->newsModel->get($id);
        $vdata = ['data' => $data];
        return view('show.html', $vdata);
    }

}
