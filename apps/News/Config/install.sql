DROP TABLE IF EXISTS `{dbprefix}news`;
CREATE TABLE `{dbprefix}news` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `catid` smallint(5) unsigned NOT NULL DEFAULT 0 COMMENT '分类id',
  `title` varchar(200) NOT NULL DEFAULT '' COMMENT '新闻名称',
  `thumb` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '封面图',
  `keywords` varchar(100) NOT NULL DEFAULT '' COMMENT '关键词',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '描述',
  `url` varchar(100) NOT NULL DEFAULT '' COMMENT '链接地址',
  `displayorder` tinyint(3) unsigned NOT NULL DEFAULT 0 COMMENT '排序',
  `status` tinyint(2) unsigned NOT NULL DEFAULT 1 COMMENT '状态0下架1正常',
  `hits` smallint(5) unsigned NOT NULL DEFAULT 0 COMMENT '点击',
  `adminid` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '管理员uid',
  `userid` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '用户uid',
  `inputtime` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '添加时间',
  `updatetime` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='新闻主表';

DROP TABLE IF EXISTS `{dbprefix}news_data`;
CREATE TABLE `{dbprefix}news_data` (
  `id` int(10) unsigned NOT NULL,
  `catid` int(8) unsigned NOT NULL DEFAULT 0,
  `content` mediumtext DEFAULT NULL COMMENT '详情'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='新闻内容';

DROP TABLE IF EXISTS `{dbprefix}news_cat`;
CREATE TABLE `{dbprefix}news_cat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '上级id',
  `name` varchar(120) NOT NULL DEFAULT '' COMMENT '名称',
  `letter` char(1) NOT NULL DEFAULT '' COMMENT '首字母',
  `enname` varchar(120) NOT NULL DEFAULT '' COMMENT '英文名称',
  `child` tinyint(1) unsigned NOT NULL DEFAULT 0 COMMENT '是否有下级',
  `childids` varchar(255) NOT NULL DEFAULT '' COMMENT '所有下级,分割 不能太多',
  `thumb` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '封面图片',
  `show` tinyint(1) unsigned NOT NULL DEFAULT 1 COMMENT '0隐藏1显示',
  `displayorder` tinyint(3) unsigned NOT NULL DEFAULT 0 COMMENT '排序',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '简介',
  `keywords` varchar(255) NOT NULL DEFAULT '' COMMENT '关键词',
  `status` tinyint(1) unsigned NOT NULL DEFAULT 0 COMMENT '状态',
  `pids` varchar(255) NOT NULL DEFAULT '' COMMENT '上级ids',
  `dirname` varchar(255) NOT NULL DEFAULT '' COMMENT '目录名称',
  `icon` varchar(255) NOT NULL DEFAULT '' COMMENT 'icon',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='新闻分类';