<?php

return [

    'admin' => [
        [
            'name' => '新闻管理',
            'mark' => 'news',
            'icon' => 'layui-icon layui-icon-picture-fine',
            'child' => [
                [
                    'name' => '新闻列表',
                    'uri' => 'news/admin/home/index',
                    'icon' => 'layui-icon layui-icon-table',
                ],
                [
                    'name' => '分类列表',
                    'uri' => 'news/admin/category/index',
                    'icon' => 'layui-icon layui-icon-table',
                ],

            ],
        ],
    ],

    'user' => [

    ],
];
