<?php

return [

    [
        'name' => '新闻模块规则',
        'uri' => '',
        'child' => [
            [
                'name' => '新闻管理',
                'uri' => '',
                'skip' => '0',
                'child' => [
                    [
                        'name' => '新闻列表',
                        'uri' => 'news/admin/home/index',
                        'skip' => '0',
                    ],
                    [
                        'name' => '添加新闻',
                        'uri' => 'news/admin/home/add',
                        'skip' => '0',
                    ],
                    [
                        'name' => '修改新闻',
                        'uri' => 'news/admin/home/edit',
                        'skip' => '0',
                    ],
                    [
                        'name' => '删除新闻',
                        'uri' => 'news/admin/home/del',
                        'skip' => '0',
                    ],
                    [
                        'name' => '新闻分类',
                        'uri' => 'news/admin/home/catindex',
                        'skip' => '0',
                    ],
                    [
                        'name' => '添加新闻分类',
                        'uri' => 'news/admin/home/catadd',
                        'skip' => '0',
                    ],
                    [
                        'name' => '修改新闻分类',
                        'uri' => 'news/admin/home/catedit',
                        'skip' => '0',
                    ],
                    [
                        'name' => '删除新闻分类',
                        'uri' => 'news/admin/home/catdel',
                        'skip' => '0',
                    ],
                    [
                        'name' => '新闻推荐',
                        'uri' => 'news/admin/home/flags',
                        'skip' => '0',
                    ],
                ],
            ],
        ],
    ],

];
