<?php
namespace App\Controllers;

/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 *
 * @package CodeIgniter
 */

use App\Controllers\FrontBase;

class UserBase extends FrontBase
{

    /**
     * An array of helpers to be loaded automatically upon
     * class instantiation. These helpers will be available
     * to all other controllers that extend BaseController.
     *
     * @var array
     */
    protected $userCongfig;

    /**
     * Constructor.
     */
    public function initController($request, $response, $logger)
    {
        // Do Not Edit This Line
        parent::initController($request, $response, $logger);

        \CodeIgniter\Events\Events::trigger('user_controller_init');

    }

    //提示消息
    public function userMsg($code, $msg, $data = [])
    {
        $url = $data['url'] ?? ''; //跳转的地址
        $time = isset($data['time']) ? intval($data['time']) : 3; //默认3秒跳转

        if ($url) {
            $backurl = $this->xss_clean($url);
            if (strpos($backurl, '?backurl=') !== false) {
                $arr = explode('?backurl=', $backurl);
                if (isset($arr[1]) && $arr[1]) {
                    $backurl = $arr[0] . '?backurl=' . urlencode($arr[1]);
                }
            }
        } else if (isset($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], 'php?') !== false) {
            $backurl = $this->xss_clean($_SERVER['HTTP_REFERER']);
        } else if (!$backurl) {
            $backurl = SELF;
        }

        $this->request->getGet('callback') && exit($this->_jsonp($code, $msg));
        if ($this->request->getGet('is_ajax') || IS_AJAX || $this->request->isAJAX()) {
            exit($this->_json($code, $msg));
        }

        $vdata = [
            'code' => $code,
            'msg' => $msg,
            'url' => $backurl,
            'time' => $time,
            'mark' => $code,
        ];
        echo view('user/msg.html', $vdata);
        exit();
    }

}
