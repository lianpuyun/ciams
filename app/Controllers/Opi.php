<?php
namespace App\Controllers;

class Opi extends BaseController
{

    public function captcha()
    {
        $captchaLib = new \App\Libraries\Captcha();
        $height = $this->request->getGet('height');
        $width = $this->request->getGet('width');
        $code = $captchaLib->create($width, $height);
        $this->session->set('amscaptcha', $code);
        exit();
    }

}
