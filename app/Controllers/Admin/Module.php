<?php
namespace App\Controllers\Admin;

use App\Controllers\AdminBase;
use App\Models\ModuleModel;

class Module extends AdminBase
{
    protected $moduleModel;

    public function __construct()
    {
        $this->moduleModel = new ModuleModel();
    }

    public function index()
    {
        if (IS_POST) {
            $page = $this->request->getPost('page');
            $limit = $this->request->getPost('limit');
            $param = $this->request->getPost();
            ams_clean_xss($param);
            list($list, $total, $param) = $this->moduleModel->limit_page($page, $limit, 0, $param);
            $return = [
                'total' => $total,
                'data' => $list,
            ];
            exit(ams_json(0, 'ok', $return));
        }
        $vdata = [];
        return view('module/index.html');
    }

    //安装
    public function install()
    {
        if (IS_POST) {
            $dirname = $this->request->getPost('dirname');
            $rt = $this->moduleModel->install($dirname);
            if ($rt['code'] == 0) {
                $this->addSystemLog('安装应用:# ' . $dirname);
            }
            $this->_json($rt);
        }
        $list = $this->moduleModel->getModuleList();
        $vdata = [
            'list' => $list,
        ];
        return view('module/install.html', $vdata);
    }

    //卸载
    public function uninstall()
    {
        if (IS_POST) {
            $dirname = $this->request->getPost('dirname');
            $rt = $this->moduleModel->uninstall($dirname);
            if ($rt['code'] == 0) {
                $this->addSystemLog('卸载应用:# ' . $dirname);
            }
            $this->_json($rt);
        }
    }

    //修改
    public function edit()
    {
        $id = intval($this->request->getGet('id'));
        if (IS_POST) {
            $do = $this->request->getPost('do');
            switch ($do) {
                case 'status':
                    $id = $this->request->getPost('id');
                    $value = $this->request->getPost('value');
                    $id = intval($id);
                    $value = ($value == 1) ? 1 : 0;

                    $rt = $this->moduleModel->switchStatus($id, $value);
                    if ($rt['code'] == 0) {
                        if ($value == 1) {
                            $this->addSystemLog('禁用应用:#' . $id);
                        } else {
                            $this->addSystemLog('启用应用:#' . $id);
                        }
                    }
                    $this->_json($rt['code'], $rt['msg']);
                    break;

                default:
                    $data = $this->request->getPost('data');
                    $rt = $this->moduleModel->edit($id, $data);
                    if ($rt['code'] == 0) {
                        $this->addSystemLog('修改应用: ' . $data['name']);
                        $this->_json(0, '操作成功');
                    }
                    $this->_json(1, $rt['msg']);
                    break;
            }

        }

        $data = $this->moduleModel->get($id);
        $vdata = [
            'data' => $data,
        ];
        return view('module/add.html', $vdata);
    }

    //删除
    public function del()
    {
        $yes = $no = 0;
        if (IS_POST) {
            $this->_json(1, '禁止删除');
            $ids = $this->request->getPost('ids');
            if ($ids) {
                if (is_array($ids)) {
                    foreach ($ids as $id) {
                        $rt = $this->moduleModel->del($id);
                        if ($rt['code'] == 0) {
                            $this->addSystemLog('删除应用:#' . $id);
                            $yes++;
                        } else {
                            $no++;
                        }
                    }
                } else {
                    $rt = $this->moduleModel->del($ids);
                    if ($rt['code'] == 0) {
                        $this->addSystemLog('删除应用:#' . $ids);
                        $yes++;
                    } else {
                        $no++;
                    }
                }
            } else {
                $this->_json(1, '参数错误');
            }
            $this->_json(0, '操作结果 成功：' . $yes . ',失败' . $no);
        }
        $this->_json(1, isset($rt['msg']) ? $rt['msg'] : '操作失败');
    }

}
