<?php

namespace App\Controllers\Admin;

use App\Controllers\AdminBase;

class System extends AdminBase
{

    public function index()
    {
        $sitefile = WRITEPATH . 'config/site.php';
        $data = require $sitefile;

        if (IS_POST) {
            $post = $this->request->getPost('data');
            foreach ($post as $key => $value) {
                $value = isset($post[$key]) ? $post[$key] : 0;
                if ($value === 'TRUE') {
                    $value = 1;
                }
                $data[$key] = $value;
            }
            $this->systemModel->save_config('site', $data);
            $this->addSystemLog('修改系统配置'); // 记录日志
            $this->adminMsg(0, '操作成功，正在刷新...', ['url' => url('admin/system/index')]);
        }

        $files = directory_map(WEBPATH . 'assets/mokui/watermark/', 1);
        $opacity = [];
        foreach ($files as $t) {
            if (substr($t, -3) == 'ttf') {
                $font[] = $t;
            } else {
                $opacity[] = $t;
            }
        }

        $this->renderer->setData([
            'data' => $data,
            'wm_opacity' => $opacity,
            'wm_font_path' => $font,
            'wm_vrt_alignment' => ['top', 'middle', 'bottom'],
            'wm_hor_alignment' => ['left', 'center', 'right'],
        ]);
        return view('system/index.html');
    }

    /**
     * 系统操作日志
     */
    public function oplog()
    {

        if (IS_POST) {
            $page = $this->request->getPost('page');
            $limit = $this->request->getPost('limit');
            $param = $this->request->getPost();
            list($list, $total, $param) = $this->systemModel->oplog_limit_page($page, $limit, 0, $param);
            if ($list) {
                foreach ($list as $k => $v) {
                    $list[$k]['inputtime'] = date('Y-m-d H:i:s', $v['inputtime']);
                }
            }

            $return = [
                'total' => $total,
                'data' => $list,
            ];
            exit(ams_json(0, 'ok', $return));
        }
        $this->renderer->setData([
        ]);
        return view('system/oplog.html');
    }

    /**
     * debug
     */
    public function debug()
    {

        $time = $this->request->getGet('time');
        $time = isset($time) ? $time : date('Y-m-d');
        $file = WRITEPATH . 'logs/log-' . $time . '.log';
        if (is_file($file)) {
            $data = trim(file_get_contents($file));
        } else {
            $data = '';
        }

        $this->renderer->setData([
            'time' => $time,
            'data' => $data,
        ]);
        return view('system/debug.html');
    }
    

    //项目设置
    public function program()
    {
        $data = $this->systemModel->get_setting('program');
        if (IS_POST) {
            $post = $this->request->getPost('data');
            $this->systemModel->save_config('program', $post);
            $this->addSystemLog('修改项目配置'); // 记录日志
            $this->adminMsg(0, '操作成功，正在刷新...', ['url' => url('admin/system/program')]);
        }
        $vdata=[
            'data' => $data,
        ];
        return view('system/program.html', $vdata);
    }


}
