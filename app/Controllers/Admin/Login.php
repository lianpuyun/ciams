<?php
namespace App\Controllers\Admin;

use App\Controllers\AdminBase;

class Login extends AdminBase
{

    public function index()
    {
        $hashcode = $this->request->getGet('hashcode');
        if (SITE_ADMIN_HASHCODE != $hashcode || !$hashcode) {
            $this->goTo404('页面未找到');
            exit();
        }

        $backurl = $this->request->getGet('backurl');
        $url = isset($backurl) ? urldecode($backurl) : url('admin/home/index');

        if (IS_POST) {
            $data = $this->request->getPost(null, FILTER_SANITIZE_STRING);
            if (!$this->check_captcha('imgcode')) {
                $this->_json(1, '验证码不正确');
            } elseif (empty($data['username']) || empty($data['password'])) {
                $this->_json(1, '账号或密码必须填写');
            } else {
                $rt = $this->adminModel->login($data['username'], $data['password']);
                if ($rt['code'] == 0) {
                    // 登录成功
                    $this->addSystemLog('登录后台成功');
                    $this->_json(0, '后台登录成功...', ['url' => $this->xss_clean($url)]);
                } else {
                    // 写入日志
                    $this->addSystemLog($rt['msg'] . '（密码' . $data['password'] . '）', $data);
                    $this->_json(1, $rt['msg']);
                }
            }
        }
        $vdata = ['hashcode' => $hashcode];
        return view('login.html', $vdata);
    }

    public function out()
    {
        $this->adminModel->logout();
        $this->adminMsg(0, '成功安全退出系统');
    }
}
