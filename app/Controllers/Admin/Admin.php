<?php
namespace App\Controllers\Admin;

use App\Controllers\AdminBase;

class Admin extends AdminBase
{

    public function __construct()
    {
    }

    /**
     * 用户列表
     */
    public function index()
    {

        $groupid = $this->request->getGet('groupid');
        $groupid = intval($groupid);

        if (IS_POST) {
            $page = $this->request->getPost('page');
            $limit = $this->request->getPost('limit');
            $param = $this->request->getPost();
            list($list, $total, $param) = $this->adminModel->limit_page($page, $limit, 0, $param);

            $return = [
                'total' => $total,
                'data' => $list,
            ];
            exit(ams_json(0, 'ok', $return));
        }
        $vdata = [
            'groupid' => $groupid,
        ];
        return view('user/index.html', $vdata);
    }

    /**
     * 添加
     */
    public function add()
    {
        if (IS_POST) {
            $data = $this->request->getPost('data');
            $rt = $this->adminModel->register($data);
            if ($rt['code'] == 0) {
                $this->addSystemLog('新增用户: [' . $rt['data']);
            }
            $this->_json($rt);
        }
        $groups = $this->adminModel->getGroupList();
        $vdata = [
            'groups' => $groups,
        ];
        return view('user/add.html', $vdata);
    }

    public function my()
    {
        $id = $this->adminid;
        $data = $this->adminModel->get($id);
        if (IS_POST) {
            $data = $this->request->getPost('data');
            $rt = $this->adminModel->edit($id, $data);
            if ($rt['code'] == 0) {
                $this->addSystemLog('修改用户: ' . $rt['data']['id']);
                $this->_json(0, '操作成功', $rt['data']);
            }
            $this->_json($rt);
        }
        $groups = $this->adminModel->getGroups();
        $vdata = [
            'groups' => $groups,
            'data' => $data,
            'id' => $id,
        ];
        return view('user/add.html', $vdata);
    }

    /**
     * 修改
     */
    public function edit()
    {
        $userid = $this->request->getGet('userid');
        $data = $this->adminModel->getUserById($userid);
        if (IS_POST) {
            $data = $this->request->getPost('data');
            $rt = $this->adminModel->edit($userid, $data);
            if ($rt['code'] == 0) {
                $this->addSystemLog('修改用户: ' . $userid);
                $this->_json(0, '操作成功');
            }
            $this->_json(1, $rt['msg']);
        }
        $groups = $this->adminModel->getGroupList();

        $vdata = [
            'groups' => $groups,
            'data' => $data,
            'userid' => $userid,
        ];
        return view('user/add.html', $vdata);
    }

    //删除
    public function del()
    {
        $yes = $no = 0;
        if (IS_POST) {
            $ids = $this->request->getPost('ids');
            ams_clean_xss($ids);
            if ($ids) {
                if (is_array($ids)) {
                    foreach ($ids as $id) {
                        $rt = $this->adminModel->del($id);
                        if ($rt['code'] == 0) {
                            $this->addSystemLog('删除用户:#' . $id . '#' . $rt['data']['username']);
                            $yes++;
                        } else {
                            $no++;
                        }
                    }
                } else {
                    $rt = $this->adminModel->del($ids);
                    if ($rt['code'] == 0) {
                        $this->addSystemLog('删除用户:#' . $ids . '#' . $rt['data']['username']);
                        $yes++;
                    } else {
                        $no++;
                    }
                }
            } else {
                $this->_json(1, '参数错误');
            }
            $this->_json(0, '操作结果 成功：' . $yes . ',失败' . $no);
        }
    }

    //授权用户
    public function oauth()
    {
        $groupid = $this->request->getGet('groupid');
        $groupid = intval($groupid);

        if (IS_POST) {
            $page = $this->request->getPost('page');
            $limit = $this->request->getPost('limit');
            $param = $this->request->getPost();
            list($list, $total, $param) = $this->adminModel->limit_page($page, $limit, 0, $param);

            $return = [
                'total' => $total,
                'data' => $list,
            ];
            exit(ams_json(0, 'ok', $return));
        }
        $vdata = [
            'groupid' => $groupid,
        ];
        return view('user/index.html', $vdata);
    }

    //分组列表管理
    public function group()
    {
        if (IS_POST) {
            $page = $this->request->getPost('page');
            $limit = $this->request->getPost('limit');
            $param = $this->request->getPost();
            $list = $this->adminModel->getGroupList($param);
            $return = [
                'total' => count($list),
                'data' => $list,
            ];
            $this->_json(0, 'ok', $return);
        }
        $vdata = [];
        return view('user/group.html', $vdata);
    }

    //添加分组
    public function addGroup()
    {
        if (IS_POST) {
            $data = $this->request->getPost('data');
            $rt = $this->adminModel->addGroup($data);
            if ($rt['code'] == 0) {
                $this->addSystemLog('添加用户分组:' . $rt['data']['id']);
            }
            $this->_json($rt['code'], $rt['msg'], $rt['data']);
        }
        $groups = $this->adminModel->getGroupList();
        $vdata = [
            'groups' => $groups,
        ];
        return view('user/group_add.html', $vdata);
    }

    //修改分组
    public function editGroup()
    {
        $groupid = $this->request->getGet('id');
        if (IS_POST) {
            $data = $this->request->getPost('data');
            $rt = $this->adminModel->editGroup($groupid, $data);
            if ($rt['code'] == 0) {
                $this->addSystemLog('修改用户分组:' . $groupid);
            }
            $this->_json($rt['code'], $rt['msg'], $rt['data']);
        }
        $groups = $this->adminModel->getGroupList();
        $data = $this->adminModel->getGroup($groupid);
        $vdata = [
            'groupid' => $groupid,
            'data' => $data,
            'groups' => $groups,
        ];
        return view('user/group_add.html', $vdata);
    }

    //删除分组
    public function delGroup()
    {
        $yes = $no = 0;
        if (IS_POST) {
            $ids = $this->request->getPost('ids');
            if ($ids) {
                if (is_array($ids)) {
                    foreach ($ids as $id) {
                        $rt = $this->adminModel->delGroup($id);
                        if ($rt['code'] == 0) {
                            $this->addSystemLog('删除用户组:#' . $id . '#' . $rt['data']['name']);
                            $yes++;
                        } else {
                            $no++;
                        }
                    }
                } else {
                    $rt = $this->adminModel->delGroup($ids);
                    if ($rt['code'] == 0) {
                        $this->addSystemLog('删除用户组:#' . $ids . '#' . $rt['data']['name']);
                        $yes++;
                    } else {
                        $no++;
                    }
                }
            } else {
                $this->_json(1, '参数错误');
            }
            $this->_json(0, '操作结果 成功：' . $yes . ',失败' . $no);
        }
    }

    //设置分组
    public function setGroup()
    {
        $vdata = [];
        return view('user/group_set.html', $vdata);
    }

    //等级列表
    public function level()
    {
        $groupid = $this->request->getGet('groupid');
        if (IS_POST) {
            # code...
        }
        $group = $this->adminModel->getGroupList($groupid);
        $vdata = [
            'group' => $group,
        ];
        return view('user/level.html', $vdata);
    }

    //添加等级
    public function addLevel()
    {
        $groupid = $this->request->getGet('groupid');
        if (IS_POST) {
            $data = $this->request->getPost('data');
            $data['groupid'] = $groupid;
            $rt = $this->adminModel->addLevel($data);
            $this->_json($rt);
        }
        $group = $this->adminModel->getGroup($groupid);
        $vdata = [
            'group' => $group,
            'groupid' => $groupid,
        ];
        return view('user/level_add.html', $vdata);
    }

    //修改等级
    public function editLevel()
    {
        $groupid = $this->request->getGet('groupid');
        if (IS_POST) {
            $data['groupid'] = $groupid;
            $data = $this->request->getPost('data');
            $rt = $this->adminModel->addLevel($data);
            $this->_json($rt);
        }
        $group = $this->adminModel->getGroup($groupid);
        $vdata = [
            'group' => $group,
            'groupid' => $groupid,
        ];
        return view('user/level_add.html', $vdata);
    }

    public function delLevel()
    {
        if (IS_POST) {
            $data = $this->request->getPost('data');
            $rt = $this->adminModel->addLevel($data);
            $this->_json($rt);
        }
        $vdata = [];
        return view('user/level_add.html', $vdata);
    }

}
