<?php
namespace App\Controllers\Admin;

use App\Controllers\AdminBase;
use App\Models\MenuModel;

class Menu extends AdminBase
{

    protected $menuModel;

    public function __construct(...$param)
    {
        $this->menuModel = new MenuModel();
    }

    public function index()
    {
        if (IS_POST) {
            $page = $this->request->getPost('page');
            $limit = $this->request->getPost('limit');
            $param = $this->request->getPost();
            list($list, $total, $param) = $this->menuModel->limit_page($page, $limit, 0, $param);
            $return = [
                'total' => $total,
                'data' => $list,
            ];
            exit(ams_json(0, 'ok', $return));
        }
        return view('menu/index.html');
    }

    //删除
    public function del()
    {
        if (IS_POST) {
            $ids = $this->request->getPost('ids', FILTER_SANITIZE_STRING);
            !$ids && exit(ams_json(1,'你还没有选择呢'));

            $this->menuModel->_delete('admin', $ids);
            $this->addSystemLog('删除后台菜单: ' . @implode(',', $ids));
            exit(ams_json(0, '操作成功', ['ids' => $ids]));
        }
        exit(ams_json(1, '操作失败'));
    }

    //添加
    public function add()
    {
        $pid = intval($this->request->getGet('pid'));
        if (IS_AJAX_POST) {
            $data = $this->request->getPost('data');
            $rt = $this->menuModel->add('admin', $pid, $data);
            if ($rt['code'] == 0) {
                $this->addSystemLog('添加后台菜单: ' . $data['name']);
                $this->_json(0, '操作成功');
            } else {
                $this->_json(1, $rt['msg']);
            }
        }
        $top = $this->menuModel->get_top('admin');
        $data = [];
        $vdata = [
            'pid' => $pid,
            'top' => $top,
            'data' => $data,
            'form' => csrf_field(),
        ];
        return view('menu/add.html', $vdata);
    }

    //修改
    public function edit()
    {

        $id = intval($this->request->getGet('id'));

        $data = $this->menuModel->getRowData('admin', $id);
        if (!$data) {
            $this->_json(0, '数据#'.$id.'不存在');
        }

        $pid = intval($data['pid']);
        $top = $this->menuModel->get_top('admin');

        if (IS_AJAX_POST) {
            $data = $this->request->getPost('data');
            $rt = $this->menuModel->edit('admin', $id, $data);
            if ($rt) {
                $this->addSystemLog('修改后台菜单: ' . $id);
                exit($this->_json(0, '操作成功', ['id' => $id]));
            }
            exit(ams_json(1, '操作失败', ['id' => $id]));
        }

        $vdata = [
            'pid' => $pid,
            'top' => $top,
            'data' => $data,
            'id' => $id,
            'form' => csrf_field(),
        ];
        return view('menu/add.html', $vdata);
    }


    public function switchHidden()
    {
        if (IS_POST) {
            $id= $this->request->getPost('id');
            $value= $this->request->getPost('value');
            $id = intval($id);
            $value = ($value==1) ? 1 : 0;
            $rt = $this->menuModel->switchHidden($id,$value);
            if ($rt['code']==0) {
                if($value==1){
                    $this->addSystemLog('显示菜单:#' . $id);
                }else{
                    $this->addSystemLog('隐藏菜单:#' . $id);
                }
            }
            $this->_json($rt['code'], $rt['msg']);
        }
    }
}
