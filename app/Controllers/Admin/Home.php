<?php

namespace App\Controllers\Admin;

use App\Controllers\AdminBase;
use App\Models\MenuModel;

class Home extends AdminBase
{

    /**
     * 重置
     */
    public function home()
    {
        $this->index();
    }

    /**
     * 首页
     */
    public function index()
    {
        $admin = $this->admin;
        $menu = cache('menu-admin-' . $admin['roleid']);
        if (!$menu) {
            $menuModel = new MenuModel();
            $menus = $menuModel->cacheData();
            $menu = $menus['admin'];
        }

        $vdata = ['menu' => $menu];
        return view('index.html', $vdata);
    }

    /**
     * 后台首页
     */
    public function main()
    {

        if (is_file(WRITEPATH . 'install.new')) {
            @unlink(WRITEPATH . 'install.new');
            return;
        }

        $server = @explode(' ', strtolower($_SERVER['SERVER_SOFTWARE']));
        if (isset($server[0]) && $server[0]) {
            $server = $server[0];
        } else {
            $server = 'web';
        }
        $this->renderer->setData([
            'serveros' => php_uname(),
            'serverip' => isset($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : '未知',
            'server' => ucfirst($server),
            'sqlversion' => $this->systemModel->get_mysql_version(),
        ]);
        return view('main.html');
    }

    /**
     * 更新全站缓存
     */
    public function cache()
    {
        $this->addSystemLog('更新全站缓存');
        $this->template->assign([
            'app' => [],
            'list' => $this->_cache_url(),
        ]);
        return view('cache.html');
    }

    // 清除缓存数据
    public function clear()
    {
        if (IS_AJAX || $this->request->getGet('todo')) {
            $this->_clear_data();
            if (!IS_AJAX) {
                $this->adminMsg(0, '全站缓存数据更新成功');
            }
        } else {
            $this->adminMsg(0, 'Clear ... ', ['url' => url('home/clear', ['todo' => 1])]);
        }
    }

    // 清除缓存数据
    private function _clear_data()
    {
        // 删除全部缓存文件
        // 重置Zend OPcache
        function_exists('opcache_reset') && opcache_reset();
    }

    public function icon()
    {
        return view('icon.html');
    }
}
