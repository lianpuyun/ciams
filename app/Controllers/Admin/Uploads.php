<?php namespace App\Controllers\Admin;

use App\Controllers\AdminBase;
use App\Models\AttachmentModel;
use App\Models\UploadsModel;

class Uploads extends AdminBase
{
    protected $uploadsModel;
    protected $attachmentModel;

    public function __construct(...$param)
    {
        $this->uploadsModel = new UploadsModel();
        $this->attachmentModel = new AttachmentModel();
    }

    //上传地址管理
    public function index()
    {
        if (IS_POST) {
            $page = $this->request->getPost('page');
            $limit = $this->request->getPost('limit');
            $param = $this->request->getPost();
            list($list, $total, $param) = $this->uploadsModel->limit_page($page, $limit, 0, $param);
            $return = [
                'total' => $total,
                'data' => $list,
            ];
            exit(ams_json(0, 'ok', $return));
        }
        $vdata = [];
        return view('uploads/index.html', $vdata);
    }

    //添加上传地址
    public function add()
    {
        if (IS_POST) {
            $data = $this->request->getPost('data');
            $rt = $this->uploadsModel->add($data);
            if ($rt['code'] == 0) {
                $this->addSystemLog('添加上传地址: ' . $data['name']);
                $this->_json(0, '操作成功');
            }
            $this->_json(1, $rt['msg']);
        }
        $vdata = [];
        return view('uploads/add.html', $vdata);
    }

    //修改上传地址
    public function edit()
    {
        $id = intval($this->request->getGet('id'));
        if (IS_POST) {
            $data = $this->request->getPost('data');
            $rt = $this->uploadsModel->edit($id, $data);
            if ($rt['code'] == 0) {
                $this->addSystemLog('修改上传地址: ' . $data['name']);
                $this->_json(0, '操作成功');
            }
            $this->_json(1, $rt['msg']);
        }
        $data = $this->uploadsModel->get($id);
        $vdata = [
            'data' => $data,
        ];
        return view('uploads/add.html', $vdata);
    }

    //删除上传地址
    public function del()
    {
        $yes = $no = 0;
        if (IS_POST) {
            $ids = $this->request->getPost('ids');
            ams_clean_xss($ids);
            if ($ids) {
                if (is_array($ids)) {
                    foreach ($ids as $id) {
                        $rt = $this->uploadsModel->del($id);
                        if ($rt['code'] == 0) {
                            $this->addSystemLog('删除上传地址:#' . $id);
                            $yes++;
                        } else {
                            $no++;
                        }
                    }
                } else {
                    $rt = $this->uploadsModel->del($ids);
                    if ($rt['code'] == 0) {
                        $this->addSystemLog('删除上传地址:#' . $ids);
                        $yes++;
                    } else {
                        $no++;
                    }
                }

            } else {
                $this->_json(1, '参数错误');
            }
            $this->_json(0, '操作结果 成功：' . $yes . ',失败' . $no);
        }
    }

    //上传地址管理
    public function oss()
    {
        if (IS_POST) {
            $page = $this->request->getPost('page');
            $limit = $this->request->getPost('limit');
            $param = $this->request->getPost();
            list($list, $total, $param) = $this->uploadsModel->limit_page_oss($page, $limit, 0, $param);
            $return = [
                'total' => $total,
                'data' => $list,
            ];
            exit(ams_json(0, 'ok', $return));
        }
        $vdata = [];
        return view('uploads/oss.html', $vdata);
    }

    //添加上传地址
    public function addOss()
    {
        if (IS_POST) {
            $data = $this->request->getPost('data');
            $rt = $this->uploadsModel->addOss($data);
            if ($rt['code'] == 0) {
                $this->addSystemLog('添加OSS存储: ' . $data['name']);
                $this->_json(0, '操作成功');
            }
            $this->_json(1, $rt['msg']);
        }
        $vdata = [];
        return view('uploads/oss-add.html', $vdata);
    }

    //修改上传地址
    public function editOss()
    {
        $id = intval($this->request->getGet('id'));
        if (IS_POST) {
            $data = $this->request->getPost('data');
            $rt = $this->uploadsModel->editOss($id, $data);
            if ($rt['code'] == 0) {
                $this->addSystemLog('修改OSS存储: ' . $data['name']);
                $this->_json(0, '操作成功');
            }
            $this->_json(1, $rt['msg']);
        }
        $data = $this->uploadsModel->getOss($id);
        $vdata = [
            'data' => $data,
        ];
        return view('uploads/oss-add.html', $vdata);
    }

    //删除上传地址
    public function delOss()
    {
        $yes = $no = 0;
        if (IS_POST) {
            $ids = $this->request->getPost('ids');
            ams_clean_xss($ids);
            if ($ids) {
                if (is_array($ids)) {
                    foreach ($ids as $id) {
                        $rt = $this->uploadsModel->delOss($id);
                        if ($rt['code'] == 0) {
                            $this->addSystemLog('删除OSS:#' . $id);
                            $yes++;
                        } else {
                            $no++;
                        }
                    }
                } else {
                    $rt = $this->uploadsModel->delOss($ids);
                    if ($rt['code'] == 0) {
                        $this->addSystemLog('删除OSS:#' . $ids);
                        $yes++;
                    } else {
                        $no++;
                    }
                }

            } else {
                $this->_json(1, '参数错误');
            }
            $this->_json(0, '操作结果 成功：' . $yes . ',失败' . $no);
        }
    }

    public function delfile()
    {
        if (IS_POST) {
            $id = $this->request->getPost('id');
            $id = intval($id);
            $rt = $this->attachmentModel->del($id);
            $this->_json($rt['code'], $rt['msg']);
        }
        $this->_json(1, '提交错误');
    }

    //上传单图片
    public function imgs()
    {
        if (IS_POST || IS_AJAX_POST) {
            $file = $this->request->getFile('file');
            $postdata = $this->request->getPost();

            //ip
            $ip = $this->request->getIPAddress();
            //文件大小
            $size = $file->getSize();
            //文件后缀名没有.
            $ext = $file->getExtension();
            //文件类型
            $type = $file->getType();
            //文件名称
            $name = $file->getName();
            //mime
            $mime = $file->getMimeType();
            //临时文件目录
            $tempfile = $file->getTempName();
            //实际存储目录
            $dir = 'writable/uploads/' . date('Ym') . '/' . date('d') . '/';
            //重命名
            $newName = $file->getRandomName();
            $nfile = $dir . $newName;

            //移动文件
            if ($file->isValid() && !$file->hasMoved()) {
                $file->move(WEBPATH . $dir, $newName);
            } elseif (!$file->isValid()) {
                if ($file->getErrorString()) {
                    $this->_json(1, '上传失败', '错误:' . $file->getError());
                }
            }

            if ($file->hasMoved()) {
                $info = getimagesize(WEBPATH . $nfile);

                $thumb = '/writable/assets/file.png';
                if ($ext === 'png' || $ext === 'jpg' || $ext === 'jpeg' || $ext === 'gif') {
                    $thumb = thumb($nfile, 200, 200);
                }

                $data = [
                    'filename' => $name,
                    'fileext' => $ext,
                    'filesize' => $size,
                    'attachment' => $nfile,
                    'inputtime' => time(),
                    'fileinfo' => json_encode($info),
                    'adminid' => $this->adminid,
                    'filename2' => $newName,
                ];
                $attachmentModel = new AttachmentModel();
                $rt = $attachmentModel->add($data);
                if ($rt) {
                    $rt['data'] = ['thumb' => $thumb, 'path' => $nfile, 'src' => SITE_URL . trim($nfile, '/'), 'fileid' => $rt['data'], 'title' => $name, 'size' => $size, 'ext' => $ext];
                }
                $this->_json($rt);
            } else {
                //移动失败
                $data = ['src' => $nfile, 'title' => $name, 'size' => $size, 'ext' => $ext];
                $this->_json(1, '上传失败', $data);
            }
        }
        $this->_json(1, '上传失败:POST');
    }

    //上传其它文件
    public function files()
    {
        if (IS_POST || IS_AJAX_POST) {
            $file = $this->request->getFile('file');
            $postdata = $this->request->getPost();

            //ip
            $ip = $this->request->getIPAddress();
            //文件大小
            $size = $file->getSize();
            //文件后缀名没有.
            $ext = $file->getExtension();
            //文件类型
            $type = $file->getType();
            //文件名称
            $name = $file->getName();
            //mime
            $mime = $file->getMimeType();
            //临时文件目录
            $tempfile = $file->getTempName();
            //实际存储目录
            $dir = 'writable/uploads/' . date('Ym') . '/' . date('d') . '/';
            //重命名
            $newName = $file->getRandomName();
            $nfile = $dir . $newName;

            //移动文件
            if ($file->isValid() && !$file->hasMoved()) {
                $file->move(WEBPATH . $dir, $newName);
            } elseif (!$file->isValid()) {
                if ($file->getErrorString()) {
                    $this->_json(1, '上传失败', '错误:' . $file->getError());
                }
            }

            if ($file->hasMoved()) {
                $info = getimagesize(WEBPATH . $nfile);

                $thumb = '/writable/assets/file.png';
                if ($ext === 'png' || $ext === 'jpg' || $ext === 'jpeg' || $ext === 'gif') {
                    $thumb = thumb($nfile, 200, 200);
                }

                $data = [
                    'filename' => $name,
                    'fileext' => $ext,
                    'filesize' => $size,
                    'attachment' => $nfile,
                    'inputtime' => time(),
                    'fileinfo' => json_encode($info),
                    'adminid' => $this->admin_uid,
                    'filename2' => $newName,
                ];
                $attachmentModel = new AttachmentModel();
                $rt = $attachmentModel->add($data);
                if ($rt) {
                    $rt['data'] = ['thumb' => $thumb, 'path' => $nfile, 'src' => SITE_URL . trim($nfile, '/'), 'fileid' => $rt['data'], 'title' => $name, 'size' => $size, 'ext' => $ext];
                }
                $this->_json($rt);
            } else {
                //移动失败
                $data = ['src' => $nfile, 'title' => $name, 'size' => $size, 'ext' => $ext];
                $this->_json(1, '上传失败', $data);
            }
        }
        $this->_json(1, '上传失败:POST');
    }

}
