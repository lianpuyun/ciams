<?php

namespace App\Controllers\Admin;

use App\Controllers\AdminBase;
use App\Models\NavbarModel;

class Navbar extends AdminBase
{

    private $menu;
    public $tablename;
    public $type; //分类
    private $navbarModel;

    /**
     * 构造函数
     */
    public function __construct(...$param)
    {
        $this->navbarModel = new NavbarModel();
        $this->type = $this->navbarModel->getAllType();
    }

    /**
     * 管理列表
     */
    public function index()
    {
        if (IS_POST) {
            $page = $this->request->getPost('page');
            $limit = $this->request->getPost('limit');
            list($list, $total, $param) = $this->navbarModel->limit_page_type($page, $limit);
            $return = [
                'total' => $total,
                'data' => $list,
            ];
            exit(ams_json(0, 'ok', $return));
        }
        $vdata = [
            'cats' => $this->type,
        ];
        return view('navbar/index.html');
    }

    /**
     * 管理列表
     */
    function list() {
        $type = $this->cache->get('navbarType');
        if (!$type) {
            $type = $this->type;
        }

        $tid = $this->request->getGet('tid');
        $tid = intval($tid);
        if (!$tid || !$this->type[$tid]) {
            $this->adminMsg(1, '分类数据不存在...');
        }
        if (IS_POST) {
            // $page = $this->request->getPost('page');
            // $limit = $this->request->getPost('limit');
            // $param = $this->request->getPost();
            // ams_clean_xss($param);
            $list = $this->navbarModel->getNavbarsByTid($tid);
            $return = [
                // 'total' => $total,
                'data' => $list,
            ];
            $this->_json(0, 'ok', $return);
        }
        $this->renderer->setData([
            'type' => $this->type,
            'tid' => $tid,
        ]);
        return view('navbar/list.html');
    }

    /**
     * 添加
     */
    public function add()
    {
        $type = $this->cache->get('navbarType');
        if (!$type) {
            $type = $this->type;
        }

        $tid = (int) $this->request->getGet('tid'); //分类
        if (!$tid || !$this->type[$tid]) {
            $this->adminMsg(1, '分类数据不存在...');
        }
        if (IS_POST) {
            $data = $this->request->getPost('data');
            $data['tid'] = $tid;
            $rt = $this->navbarModel->add($data);
            if ($rt['code'] == 0) {
                $this->addSystemLog('添加导航链接【#' . $data['name'] . '】');
                $this->_json($rt['code'], $rt['msg'], $rt['data']);
            }
            $this->_json($rt['code'], $rt['msg'], $rt['data']);
        }

        $cats = $this->navbarModel->get_select($tid);
        $this->renderer->setData([
            'type' => $this->type,
            'cats' => $cats,
            'pid' => 0,
            'tid' => $tid,
        ]);
        return view('navbar/add.html');
    }

    /**
     * 修改
     */
    public function edit()
    {
        $id = (int) $this->request->getGet('id');
        $rt = $this->navbarModel->get($id);
        $data = $rt['data'];
        $tid = $data['tid'];

        if (IS_POST) {
            $data = $this->request->getPost('data');
            $data['tid'] = $tid;
            $rt = $this->navbarModel->edit($id, $data);
            if ($rt['code'] == 0) {
                $this->addSystemLog('修改导航链接【#' . $data['name'] . '】');
                $this->_json($rt['code'], $rt['msg'], $rt['data']);
            }
            $this->_json($rt['code'], $rt['msg'], $rt['data']);
        }
        $tid = $data['tid'];
        $cats = $this->navbarModel->get_select($tid);

        $this->renderer->setData([
            'data' => $data,
            'id' => $id,
            'cats' => $cats,
            'pid' => $data['pid'],
            'tid' => $tid,
        ]);
        return view('navbar/add.html');
    }

    /**
     * 显示
     */
    public function show()
    {
        if ($this->is_auth('admin/navbar/edit')) {
            $id = (int) $this->request->getGet('id');
            $data = $this->db
                ->select('show,tid')
                ->where('id', $id)
                ->limit(1)
                ->get('navbar')
                ->row_array();
            $this->db->where('id', $id)->update('navbar', ['show' => ($data['show'] == 1 ? 0 : 1)]);
            $this->cache(1);
            $this->addSystemLog('修改自定义链接【' . $this->menu[$this->type] . '#' . $id . '】'); // 记录日志
            $this->adminMsg(0, '操作成功，正在刷新...', ['url' => url('navbar/list', ['tid' => $data['tid']])]);
        } else {
            $this->adminMsg(1, '您无权限操作');
        }
    }

    public function del()
    {
        $yes = $no = 0;
        if (IS_POST) {
            $ids = $this->request->getPost('ids');
            ams_clean_xss($ids);
            if ($ids) {
                if (is_array($ids)) {
                    foreach ($ids as $id) {
                        $rt = $this->navbarModel->del($id);
                        if ($rt['code'] == 0) {
                            $this->addSystemLog('删除链接:#' . $id . '#' . $rt['data']['name']);
                            $yes++;
                        } else {
                            $no++;
                        }
                    }
                } else {
                    $rt = $this->navbarModel->del($ids);
                    if ($rt['code'] == 0) {
                        $this->addSystemLog('删除链接:#' . $ids . '#' . $rt['data']['name']);
                        $yes++;
                    } else {
                        $no++;
                    }
                }
            } else {
                $this->_json(1, '参数错误');
            }
            $this->_json(0, '操作结果 成功：' . $yes . ',失败' . $no);
        }
    }

    public function clear()
    {
        $tid = (int) $this->request->getPost('tid');
        if (!$tid || !isset($this->type[$tid])) {
            $this->_json(1, '分类不存在');
        }
        $rt = $this->navbarModel->clear($tid);
        $this->addSystemLog('清空导航分类[' . $tid . ']的数据'); // 记录日志
        $this->_json(0, '操作成功...');
    }

    //设置状态
    public function switchShow()
    {
        if (IS_POST) {
            $id = $this->request->getPost('id');
            $value = $this->request->getPost('value');
            $id = intval($id);
            $value = ($value == 1) ? 1 : 0;
            $rt = $this->navbarModel->switchShow($id, $value);
            if ($rt['code'] == 0) {
                if ($value == 1) {
                    $this->addSystemLog('隐藏链接:#' . $id);
                } else {
                    $this->addSystemLog('显示链接:#' . $id);
                }
            }
            $this->_json($rt['code'], $rt['msg']);
        }
    }

    //导航分类管理
    public function type()
    {
        if (IS_POST) {
            $page = $this->request->getPost('page');
            $limit = $this->request->getPost('limit');
            list($list, $total, $param) = $this->navbarModel->limit_page_type($page, $limit);
            $return = [
                'total' => $total,
                'data' => $list,
            ];
            exit(ams_json(0, 'ok', $return));
        }
        $vdata = [
            'cats' => $this->cats,
        ];
        return view('news/index.html', $vdata);
    }

    public function addType()
    {
        if (IS_POST) {
            $data = $this->request->getPost('data');
            $rt = $this->navbarModel->addType($data);
            if ($rt['code'] == 0) {
                $this->addSystemLog('添加导航分类[' . $data['name'] . ']'); // 记录日志
            }
            $this->_json($rt);
        }
        return view('navbar/addType.html');
    }

    public function editType()
    {
        $id = $this->request->getGet('id');
        if (IS_POST) {
            $data = $this->request->getPost('data');
            unset($data['id']);
            $rt = $this->navbarModel->editType($id, $data);
            if ($rt['code'] == 0) {
                $this->addSystemLog('添加导航分类[' . $data['name'] . ']'); // 记录日志
            }
            $this->_json($rt);
        }
        $data = $this->navbarModel->getOneType($id);
        $vdata = [
            'data' => $data,
            'id' => $id,
        ];
        return view('navbar/addType.html');
    }

    public function delType()
    {
        $yes = $no = 0;
        if (IS_POST) {
            $ids = $this->request->getPost('ids');
            ams_clean_xss($ids);
            if ($ids) {
                if (is_array($ids)) {
                    foreach ($ids as $id) {
                        $rt = $this->navbarModel->delType($id);
                        if ($rt['code'] == 0) {
                            $this->addSystemLog('删除导航分类:#' . $id);
                            $yes++;
                        } else {
                            $no++;
                        }
                    }
                } else {
                    $rt = $this->navbarModel->delType($ids);
                    if ($rt['code'] == 0) {
                        $this->addSystemLog('删除导航分类:#' . $ids);
                        $yes++;
                    } else {
                        $no++;
                    }
                }

            } else {
                $this->_json(1, '参数错误');
            }
            $this->_json(0, '操作结果 成功：' . $yes . ',失败' . $no);
        }
    }

    public function cache($update = 0)
    {
        $this->navbarModel->cacheData();
        $this->adminMsg(0, '操作成功，正在刷新...');
    }

}
