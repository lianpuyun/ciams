<?php

namespace App\Controllers\Admin;

use App\Controllers\AdminBase;
use App\Models\PagesModel;

class Pages extends AdminBase
{

    protected $pagesModel;
    public $cats;

    /**
     * 构造函数
     */
    public function __construct()
    {
        $this->pagesModel = new PagesModel();
        $this->cats = $this->pagesModel->page_get_select();
    }

    /**
     * 首页
     */
    public function index()
    {
        if (IS_POST) {
            $page = $this->request->getPost('page');
            $limit = $this->request->getPost('limit');
            // list($list, $total, $param) = $this->pagesModel->limit_page($page, $limit, 0);
            $list = $this->pagesModel->page_get_select();
            $total = isset($list) ? count($list) : 0;
            $return = [
                'total' => $total,
                'data' => $list,
            ];
            exit(ams_json(0, 'ok', $return));
        }
        $vdata = [
            'cats' => $this->cats,
        ];
        return view('pages/index.html');
    }

    /**
     * 添加
     */
    public function add()
    {
        if (IS_POST) {
            $data = $this->request->getPost('data');
            $rt = $this->pagesModel->add($data);
            if ($rt['code'] == 0) {
                $this->addSystemLog('添加单页: ' . $data['name']);
                $this->_json(0, '操作成功');
            }
            $this->_json(1, $rt['msg']);
        }
        $vdata = [
            'cats' => $this->cats,
        ];
        return view('pages/add.html', $vdata);
    }

    //修改
    public function edit()
    {
        $id = intval($this->request->getGet('id'));
        if (IS_POST) {

            $do = $this->request->getPost('do');
            switch ($do) {
                case 'sort':
                    
                    break;

                default:
                    $data = $this->request->getPost('data');
                    $rt = $this->pagesModel->edit($id, $data);
                    if ($rt['code'] == 0) {
                        $this->addSystemLog('修改单页: ' . $data['name']);
                    }
                    $this->_json($rt['code'], $rt['msg']);
                    break;
            }

        }
        $data = $this->pagesModel->get($id);
        $vdata = [
            'cats' => $this->cats,
            'data' => $data['data'],
        ];
        return view('pages/add.html', $vdata);
    }

    //删除
    public function del()
    {
        $yes = $no = 0;
        if (IS_POST) {
            $ids = $this->request->getPost('ids');
            if ($ids && is_array($ids)) {
                foreach ($ids as $id) {
                    $rt = $this->pagesModel->del($id);
                    if ($rt['code'] == 0) {
                        $this->addSystemLog('删除单页:#' . $id);
                        $yes++;
                    } else {
                        $no++;
                    }
                }
            } else {
                $this->_json(1, '参数错误');
            }
            $this->_json(0, '操作结果 成功：' . $yes . ',失败' . $no);
        }
        $this->_json(1, isset($rt['msg']) ? $rt['msg'] : '操作失败');

    }

}
