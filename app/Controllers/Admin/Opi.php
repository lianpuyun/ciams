<?php

namespace App\Controllers\Admin;

use App\Controllers\AdminBase;
use App\Models\UploadsModel;

class Opi extends AdminBase
{

    /**
     * 生成安全码
     */
    public function syskey()
    {
        $str = ams_syskey();
        $this->_json(0, 'ok', $str);
    }

    //获取城市联动
    public function get_region()
    {
        if (IS_POST) {
            $type = $this->request->getPost('type');
            $pid = $this->request->getPost('pid');
            if ($type) {
                ams_clean_xss($type);
            } elseif ($pid) {
                ams_clean_xss($pid);
            }
            if (!$type || !in_array($type, ['province', 'city', 'area'])) {
                $this->_json(1, '参数错误');
            }
            $regionModel = model('App\Models\RegionModel');
            $cache = cache('region_' . $type);
            if (!$cache) {
                $cache = $regionModel->cacheData();
                $cache = $cache[$type];
            }
            if ($pid) {
                $data = isset($cache[$pid]) ? $cache[$pid] : [];
            } else {
                $data = $cache;
            }
            $this->_json(0, 'ok', $data);
        }

    }

    /**
     * 生成来路随机字符
     */
    public function referer()
    {
        $s = strtoupper(base64_encode(md5(time()) . md5(rand(0, 2015) . md5(rand(0, 2015)))) . md5(rand(0, 2009)));
        $str = str_replace('=', '', substr($s, 0, 64));
        $this->_json(0, 'ok', $str);
    }

    //上传文件
    public function upload()
    {
        if (IS_POST || IS_AJAX_POST) {
            $file = $this->request->getFile('file');
            $postdata = $this->request->getPost();

            //ip
            $ip = $this->request->getIPAddress();
            //文件大小
            $size = $file->getSize();
            //文件后缀名没有.
            $ext = $file->getExtension();
            //文件类型
            $type = $file->getType();
            //文件名称
            $name = $file->getName();
            //mime
            $mime = $file->getMimeType();
            //临时文件目录
            $tempfile = $file->getTempName();
            //实际存储目录
            $dir = 'uploads/' . date('Ym') . '/' . date('d') . '/';
            //重命名
            $newName = $file->getRandomName();
            $nfile = $dir . $newName;

            if (!in_array($ext, ['png', 'jpg', 'jpeg', 'gif', 'bmp'])) {
                $this->_json(1, '上传失败', '错误:请上传图片');
            }

            //移动文件
            if ($file->isValid() && !$file->hasMoved()) {
                $file->move(WRITEPATH . $dir, $newName);
            } elseif (!$file->isValid()) {
                if ($file->getErrorString()) {
                    $this->_json(1, '上传失败', '错误:' . $file->getError());
                }
            }

            if ($file->hasMoved()) {
                $info = getimagesize(WRITEPATH . $nfile);
                $thumb = '/assets/mokui/image/ext/' . $ext . '.png';
                if ($ext === 'png' || $ext === 'jpg' || $ext === 'jpeg' || $ext === 'gif') {
                    $thumb = thumb($nfile, 200, 200);
                }
                $data = [
                    'filename' => $name,
                    'fileext' => $ext,
                    'filesize' => $size,
                    'attachment' => $nfile,
                    'inputtime' => time(),
                    'fileinfo' => json_encode($info),
                    'adminid' => 0,
                    'newname' => $newName,
                ];
                $this->AttachmentModel = model('App\Models\AttachmentModel');
                $rt = $this->AttachmentModel->add($data);
                if ($rt) {
                    $rt['data'] = ['thumb' => $thumb, 'src' => SITE_URL . trim($nfile, '/'), 'fileid' => $rt['data'], 'title' => $name, 'size' => $size, 'ext' => $ext];
                }
                $this->_json($rt);
            } else {
                //移动失败
                $data = ['src' => $nfile, 'title' => $name, 'size' => $size, 'ext' => $ext];
                $this->_json(1, '上传失败', $data);
            }
        }
        $this->_json(1, '上传失败:POST');
    }


}
