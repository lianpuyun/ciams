<?php

namespace App\Controllers\Admin;

use App\Controllers\AdminBase;
use App\Models\AttachmentModel;

class Attachment extends AdminBase
{
    protected $attachModel;
    public function __construct()
    {
        parent::__construct();
        $this->attachModel = new AttachmentModel();
    }

    public function index()
    {
        if (IS_POST) {
            $page = $this->request->getPost('page');
            $limit = $this->request->getPost('limit');
            list($list, $total, $param) = $this->attachModel->limit_page($page, $limit, 0);
            if ($list) {
                foreach ($list as $k => $v) {
                    $list[$k]['inputtime_data'] = date('Y-m-d H:i:s', $v['inputtime']);
                }
            }
            $return = [
                'total' => $total,
                'data' => $list,
            ];
            exit(ams_json(0, 'ok', $return));
        }
        $vdata = [];
        return view('article/index.html', $vdata);
    }

}
