<?php

namespace App\Controllers\Admin;

use App\Controllers\AdminBase;
use App\Models\BlackModel;

class Black extends AdminBase
{

    protected $blackModel;

    public function __construct()
    {
        $this->blackModel = new BlackModel();
    }

    public function index()
    {
        if (IS_POST) {
            $page = $this->request->getPost('page');
            $limit = $this->request->getPost('limit');
            $param = $this->request->getPost();
            list($list, $total, $param) = $this->blackModel->limit_page($page, $limit, 0, $param);
            $return = [
                'total' => $total,
                'data' => $list,
            ];
            $this->_json(0, 'ok', $return);
        }
        return view('black/index.html');
    }

    public function add()
    {

        if (IS_POST) {
            $data = $this->request->getPost('data');
            $rt = $this->blackModel->add($data);
            if ($rt['code'] == 0) {
                $this->addSystemLog('添加 IP黑名单: ' . $data['ip']);
                $this->_json(0, '操作成功');
            }
            $this->_json(1, $rt['msg']);
        }

        $vdata = ['myip' => $this->request->getIPAddress()];

        return view('black/add.html', $vdata);
    }

    public function edit()
    {

        $id = $this->request->getGet('id');
        if (IS_POST) {

            $do = $this->request->getPost('do');
            switch ($do) {
                case 'status':
                    $id = $this->request->getPost('id');
                    $value = $this->request->getPost('value');
                    $id = intval($id);
                    $value = ($value == 1) ? 1 : 0;

                    $rt = $this->blackModel->switchStatus($id, $value);
                    if ($rt['code'] == 0) {
                        if ($value == 1) {
                            $this->addSystemLog('取消拦截:#' . $id);
                        } else {
                            $this->addSystemLog('开启拦截:#' . $id);
                        }
                    }
                    $this->_json($rt['code'], $rt['msg']);
                    break;

                default:
                    $data = $this->request->getPost('data');
                    $rt = $this->blackModel->edit($id, $data);
                    if ($rt['code'] == 0) {
                        $this->addSystemLog('修改 IP黑名单: ' . $data['ip']);
                        $this->_json(0, '操作成功');
                    }
                    $this->_json(1, $rt['msg']);
                    break;
            }

        }
        $data = $this->blackModel->get($id);
        $data['endtime'] = $data['endtime'] ? date('Y-m-d', $data['endtime']) : '';
        $this->renderer->setData([
            'data' => $data,
            'myip' => $this->request->getIPAddress(),
        ]);
        return view('black/add.html');
    }

    public function del()
    {
        $yes = $no = 0;
        if (IS_POST) {
            $ids = $this->request->getPost('ids');
            if ($ids) {
                if (is_array($ids)) {
                    foreach ($ids as $id) {
                        $rt = $this->blackModel->del($id);
                        if ($rt['code'] == 0) {
                            $this->addSystemLog('删除黑名单IP:#' . $id);
                            $yes++;
                        } else {
                            $no++;
                        }
                    }
                } else {
                    $rt = $this->blackModel->del($ids);
                    if ($rt['code'] == 0) {
                        $this->addSystemLog('删除黑名单IP:#' . $ids);
                        $yes++;
                    } else {
                        $no++;
                    }
                }
            } else {
                $this->_json(1, '参数错误');
            }
            $this->_json(0, '操作结果 成功：' . $yes . ',失败' . $no);
        }
        $this->_json(1, isset($rt['msg']) ? $rt['msg'] : '操作失败');
    }

    public function cache()
    {
        $this->blackModel->cache();
        $this->adminMsg(0, '更新成功', ['url' => url('black/index')]);
    }

}
