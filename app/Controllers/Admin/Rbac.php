<?php

namespace App\Controllers\Admin;

use App\Controllers\AdminBase;
use App\Models\RbacModel;

class Rbac extends AdminBase
{

    protected $rbacModel;

    public function __construct()
    {
        $this->rbacModel = new RbacModel();
    }

    /*******管理员管理********/
    //管理员列表
    public function index()
    {
        if (IS_POST) {
            $page = $this->request->getPost('page');
            $limit = $this->request->getPost('limit');
            $param = $this->request->getPost();
            list($list, $total, $param) = $this->rbacModel->user_limit_page($page, $limit, 0, $param);
            if ($list) {
                foreach ($list as $k => $v) {
                    $rt = $this->rbacModel->getRole($v['roleid']);
                    $list[$k]['role_name'] = ($rt['code'] == 0) ? $rt['data']['name'] : '';
                }
            }
            $return = [
                'total' => $total,
                'data' => $list,
            ];
            exit(ams_json(0, 'ok', $return));
        }
        $this->renderer->setData([]);
        return view('rbac/user_index.html');
    }

    //添加管理员
    public function add()
    {
        $roleid = $this->request->getGet('roleid');
        $roleid = $roleid ? intval($roleid) : 0;
        if (IS_POST) {
            $post = $this->request->getPost('data');
            $rt = $this->rbacModel->addUser($post);
            if ($rt['code'] == 0) {
                $this->addSystemLog('添加管理员#' . $post['username']); // 记录日志
            }
            $this->_json($rt['code'], $rt['msg']);
        }
        $role = [];
        $rt = $this->rbacModel->getAllRole();
        if ($rt['code'] == 0) {
            $role = $rt['data'];
        }
        $this->renderer->setData([
            'roleid' => $roleid,
            'role' => $role,
        ]);
        return view('rbac/user_add.html');
    }

    //修改管理员
    public function edit()
    {
        $roleid = $this->request->getGet('roleid');
        $roleid = $roleid ? intval($roleid) : 0;
        $userid = $this->request->getGet('userid');
        $userid = $userid ? intval($userid) : 0;

        if (IS_POST) {
            $post = $this->request->getPost('data');
            $rt = $this->rbacModel->editUser($userid, $post);
            if ($rt['code'] == 0) {
                $this->addSystemLog('修改管理员#' . $post['username']); // 记录日志
            }
            $this->_json($rt['code'], $rt['msg']);
        }

        $role = [];
        $rt = $this->rbacModel->getAllRole();
        if ($rt['code'] == 0) {
            $role = $rt['data'];
        }

        $rt = $this->rbacModel->getUser($userid);
        if ($rt['code'] == 0) {
            $data = $rt['data'];
        } else {
            $data = [];
        }

        $this->renderer->setData([
            'userid' => $userid,
            'data' => $data,
            'roleid' => $roleid,
            'role' => $role,
        ]);
        return view('rbac/user_add.html');
    }

    //删除管理员
    public function del()
    {
        $yes = $no = 0;
        if (IS_POST) {
            $ids = $this->request->getPost('ids');
            if ($ids) {
                if (is_array($ids)) {
                    foreach ($ids as $id) {
                        $id = intval($id);
                        $rt = $this->rbacModel->delUser($id);
                        if ($rt['code'] == 0) {
                            $this->addSystemLog('删除管理员:#' . $id . '#' . $rt['data']['username']);
                            $yes++;
                        } else {
                            $no++;
                        }
                    }
                } else {
                    $ids = intval($ids);
                    $rt = $this->rbacModel->delUser($ids);
                    if ($rt['code'] == 0) {
                        $this->addSystemLog('删除管理员:#' . $ids . '#' . $rt['data']['username']);
                        $yes++;
                    } else {
                        $no++;
                    }
                }

            } else {
                $this->_json(1, '参数错误');
            }
            $this->_json(0, '操作结果 成功：' . $yes . ',失败' . $no);
        }

    }

    /*******角色管理********/
    //角色列表
    public function role()
    {
        if (IS_POST) {
            $page = $this->request->getPost('page');
            $limit = $this->request->getPost('limit');
            $param = $this->request->getPost();
            list($list, $total, $param) = $this->rbacModel->role_limit_page($page, $limit, 0, $param);
            $return = [
                'total' => $total,
                'data' => $list,
            ];
            exit(ams_json(0, 'ok', $return));
        }
        $this->renderer->setData([]);
        return view('rbac/role.html');
    }

    //添加角色
    public function addRole()
    {
        if (IS_POST) {
            $data = $this->request->getPost('data');
            $rt = $this->rbacModel->addRole($data);
            if ($rt['code'] == 0) {
                $this->addSystemLog('添加管理员角色'); // 记录日志
                $this->adminMsg(0, '操作成功，正在刷新...', ['url' => url('admin/rbac/index')]);
            } else {
                $this->adminMsg(1, $rt['msg'], ['url' => url('admin/rbac/index')]);
            }
        }
        $this->renderer->setData([]);
        return view('rbac/role_add.html');
    }

    //修改角色
    public function editRole()
    {
        $id = $this->request->getGet('id');
        $id = $id ? intval($id) : 0;
        $rt = $this->rbacModel->getRole($id);
        if ($rt['code'] != 0) {
            $this->adminMsg(1, '数据不存在', ['url' => url('admin/rbac/index')]);
        }
        $data = $rt['data'];
        if (IS_POST) {
            $data = $this->request->getPost('data');
            $rt = $this->rbacModel->editRole($id, $data);

            if ($rt['code'] == 0) {
                $this->addSystemLog('修改管理员角色'); // 记录日志
                $this->adminMsg(0, $rt['msg'], ['url' => url('admin/rbac/index')]);
            } else {
                $this->adminMsg(1, $rt['msg'], ['url' => url('admin/rbac/index')]);
            }
        }

        $this->renderer->setData([
            'id' => $id,
            'data' => $data,
        ]);
        return view('rbac/role_add.html');
    }

    //删除角色
    public function delRole()
    {
        if (IS_POST) {
            $id = $this->request->getPost('id');
            $id = $id ? intval($id) : 0;

            $rt = $this->rbacModel->delRole($id);
            if ($rt['code'] == 0) {
                $this->addSystemLog('删除管理员角色'); // 记录日志
                $this->adminMsg(0, '操作成功，正在刷新...', ['url' => url('admin/rbac/index')]);
            } else {
                $this->adminMsg(1, $rt['msg'], ['url' => url('admin/rbac/index')]);
            }
        }
    }

    //角色权限
    public function authRole()
    {
        $roleid = $this->request->getGet('roleid');
        $roleid = intval($roleid);
        $role = $auth = $rule = [];

        if (IS_POST) {
            $post = $this->request->getPost('data');
            $rt = $this->rbacModel->setRoleAuth($roleid, $post);
            if ($rt['code'] == 0) {
                $this->addSystemLog('设置角色权限#' . $roleid); // 记录日志
            }
            $this->_json($rt['code'], $rt['msg']);
        }

        $rt = $this->rbacModel->getRole($roleid);
        if ($rt['code'] == 0) {
            $role = $rt['data'];
            $auth = $rt['data']['auth'];
        }

        $rule = $this->rbacModel->getAllRule();

        $this->renderer->setData([
            'role' => $role,
            'roleid' => $roleid,
            'rule' => $rule, //所有规则
            'auth' => $auth, //已有权限
        ]);
        return view('rbac/role_auth.html');
    }

    //开启和关闭套餐
    public function roleStatus()
    {
        if (IS_POST) {
            $id = $this->request->getPost('id');
            $value = $this->request->getPost('value');
            $id = intval($id);
            $value = ($value == 1) ? 1 : 0;

            $rt = $this->rbacModel->switchRoleStatus($id, $value);
            if ($rt['code'] == 0) {
                if ($value == 1) {
                    $this->addSystemLog('禁用角色:#' . $id);
                } else {
                    $this->addSystemLog('启用角色:#' . $id);
                }
            }
            $this->_json($rt['code'], $rt['msg']);
        }
    }

    /*******规则管理********/
    //规则管理
    public function rule()
    {
        if (IS_POST) {
            $page = $this->request->getPost('page');
            $limit = $this->request->getPost('limit');
            $param = $this->request->getPost();
            list($list, $total, $param) = $this->rbacModel->rule_limit_page($page, $limit, 0, $param);
            $return = [
                'total' => $total,
                'data' => $list,
            ];
            exit(ams_json(0, 'ok', $return));
        }
        $parent = $this->rbacModel->getParents();
        $this->renderer->setData([
            'parent' => $parent,
        ]);
        return view('rbac/rule.html');
    }

    //添加规则
    public function addRule()
    {
        $pid = $this->request->getGet('pid');
        $pid = $pid ? intval($pid) : 0;
        if (IS_POST) {
            $data = $this->request->getPost('data');
            $pid = $data['pid'];
            unset($data['pid']);
            $yes = $no = 0;
            foreach ($data as $k => $v) {
                $rt = $this->rbacModel->addRule($pid, $v);
                if ($rt['code'] == 0) {
                    $yes++;
                } else {
                    $no++;
                }
            }
            $this->addSystemLog('添加权限规则,成功:' . $yes . ', 失败:' . $no); // 记录日志
            $this->adminMsg($no, '添加权限规则,成功:' . $yes . ', 失败:' . $no, ['url' => url('admin/rbac/rule')]);
        }
        $parent = $this->rbacModel->getParents();
        $son = $this->rbacModel->getRuleSons($pid);
        $vdata = [
            'parent' => $parent,
            'son' => $son,
            'pid' => $pid,
        ];
        return view('rbac/rule_add.html', $vdata);
    }

    //修改规则
    public function editRule()
    {
        $id = $this->request->getGet('id');
        $id = $id ? intval($id) : 0;
        $parent = $this->rbacModel->getParents();

        $row = $this->rbacModel->getRule($id);
        $data[] = $row;
        $pid = $row['pid'];

        if (IS_POST) {
            $data = $this->request->getPost('data');
            $pid = $data['pid'];
            unset($data['pid']);
            if (count($data) < 1) {
                exit(ams_json(1, '参数失败'));
            }
            $yes = $no = 0;
            foreach ($data as $k => $v) {
                $v['pid'] = $pid;
                $rt = $this->rbacModel->editRule($id, $v);
                if ($rt['code'] == 0) {
                    $yes++;
                } else {
                    $no++;
                }
            }
            $this->addSystemLog('修改权限规则,成功:' . $yes . ', 失败:' . $no); // 记录日志
            $this->adminMsg($no, '修改权限规则,成功:' . $yes . ', 失败:' . $no, ['url' => url('admin/rbac/rule')]);
        }

        $this->renderer->setData([
            'pid' => $pid,
            'id' => $id,
            'parent' => $parent,
            'data' => $data,
        ]);
        return view('rbac/rule_add.html');
    }

    //删除规则
    public function delRule()
    {
        $yes = $no = 0;
        if (IS_POST) {
            $ids = $this->request->getPost('ids');
            if ($ids) {
                if (is_array($ids)) {
                    foreach ($ids as $id) {
                        $id = intval($id);
                        $rt = $this->rbacModel->delRule($id);
                        if ($rt['code'] == 0) {
                            $this->addSystemLog('删除权限规则:#' . $id . '#' . $rt['data']['name']);
                            $yes++;
                        } else {
                            $no++;
                        }
                    }
                } else {
                    $ids = intval($ids);
                    $rt = $this->rbacModel->delRule($ids);
                    if ($rt['code'] == 0) {
                        $this->addSystemLog('删除权限规则:#' . $ids . '#' . $rt['data']['name']);
                        $yes++;
                    } else {
                        $no++;
                    }
                }

            } else {
                $this->_json(1, '参数错误');
            }
            $this->_json(0, '操作结果 成功：' . $yes . ',失败' . $no);
        }

    }

}
