<?php

function url($uri, $query = null, $self = SELF)
{
    if (!$uri) {
        return $self;
    }

    $uri = trim($uri, '/');

    if ($self == 'index.php') {
        $uri = '/' . $uri;
    } else {
        $uri = '/' . $self . '/' . $uri;
    }

    $url = $uri;

    if (is_array($query) && $query) {
        $param = @http_build_query($query);
    } else {
        $param = $query;
    }
    if ($param) {
        return $url . '?' . $param;
    }
    return $url;
}

/**
 * 获取随机字符串
 * @param int $randLength 长度
 * @param int $addtime 是否加入当前时间戳
 * @param int $includenumber 是否包含数字
 * @return string
 */
function ams_randstr($randLength = 12, $addtime = 1, $includenumber = 1)
{
    $tokenvalue = '';
    if ($includenumber) {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHJKLMNPQESTUVWXYZ123456789';
    } else {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHJKLMNPQESTUVWXYZ';
    }
    $len = strlen($chars);
    $randStr = '';
    for ($i = 0; $i < $randLength; $i++) {
        $randStr .= $chars[mt_rand(0, $len - 1)];
    }
    $tokenvalue = $randStr;
    if ($addtime) {
        $tokenvalue = $randStr . time();
    }
    return $tokenvalue;
}

//生成盐
function ams_salt()
{
    $str = substr(md5(rand(123, 999)), 0, 10);
    return $str;
}

function ams_rt($code = 0, $msg = 'ok', $data = null)
{
    $rt = [
        'code' => $code,
        'msg' => $msg ? esc($msg) : '',
        'data' => $data,
    ];
    return $rt;
}

function ams_json($code = 1, $msg = '', $data = null)
{
    $data = [
        'code' => intval($code),
        'msg' => $msg ? esc($msg) : '',
        'data' => isset($data) ? $data : null,
    ];
    echo json_encode($data);
    exit();
}

function ams_isphone($phone)
{
    if (!is_numeric($phone) || !$phone || strlen($phone) != 11) {
        return false;
    }
    $isMob = "/^1[3456789]\d{9}$/";
    return preg_match($isMob, $phone) ? true : false;
}

//域名补上协议
function ams_http_prefix($url)
{
    if (defined(SITE_HTTPS) && SITE_HTTPS) {
        $url = 'https://' . $url;
    } else {
        $url = 'http://' . $url;
    }
    return $url;
}

/**
 * @param $val 需要过滤值
 * @return string
 */
function xss_clean($val)
{
    $val = strip_tags($val);
    $val = htmlspecialchars($val, ENT_QUOTES);
    return $val;
}

function ams_syskey()
{
    $str = 'CIAMS' . strtoupper(substr((md5(mt_rand(111, 999) . time())), rand(0, 10), 13));
    return $str;
}

function ams_clean_xss(&$string, $low = false)
{
    if (!is_array($string)) {
        $string = trim($string);
        $string = strip_tags($string);
        $string = htmlspecialchars($string);
        if ($low) {
            return $string;
        }
        $string = str_replace(['"', "\\", "'", "/", "..", "../", "./", "//"], '', $string);
        $no = '/%0[0-8bcef]/';
        $string = preg_replace($no, '', $string);
        $no = '/%1[0-9a-f]/';
        $string = preg_replace($no, '', $string);
        $no = '/[\x00-\x08\x0B\x0C\x0E-\x1F\x7F]+/S';
        $string = preg_replace($no, '', $string);
        return $string;
    }
    $keys = array_keys($string);
    foreach ($keys as $key) {
        ams_clean_xss($string[$key]);
    }
    return $string;
}

/**
 * 验证码图片获取
 */
function ams_captcha($width = 120, $height = 40, $url = '')
{
    $url = url('opi/captcha', ['width' => $width, 'height' => $height], 'index.php');
    return '<img align="absmiddle" alt="点击更换验证码" style="border:1px solid #9a9a9a;cursor:pointer" title="点击更换验证码" class="imgcaptcha" style="cursor:pointer;" onclick="this.src=\'' . $url . '&\'+Math.random();" src="' . $url . '" />';
}

/*
 * 源图的路径 img 也可以是远程图片
 * 最终保存图片的宽 width
 * 最终保存图片的高 height
 */
function thumb($img = null, $width = 0, $height = 0, $water = 0)
{
    $thumb_path = WEBPATH . 'uploads/thumb/';
    if (is_dir(WRITEPATH . 'uploads/thumb/')) {
        ams_mkdirs(WRITEPATH . 'uploads/thumb/');
    }
    
    $default = SITE_URL . 'assets/mokui/image/no-img.png';
    $thumb_file = $thumb_path . md5($img) . '_' . $width . 'x' . $height . '_' . $water . '.png';

    if (!$img) {
        return $default;
    }

    $img = WEBPATH . trim($img, '/');
    //是否存在
    if (is_file($thumb_file)) {
        $thumb_file = str_replace(WEBPATH, '/', $thumb_file);
        return SITE_URL . trim($thumb_file, '/');
    } elseif (!is_file($img)) {
        return $default;
    }

    //源图对象
    $src_image = imagecreatefromstring(file_get_contents($img));
    $src_width = imagesx($src_image);
    $src_height = imagesy($src_image);
    //缩放比例计算
    if (!$width && !$height) {
        $width = $src_width;
        $height = $src_height;
    }

    if ($width > $src_width) {
        $width = $src_width;
    }
    if ($height > $src_height) {
        $height = $src_height;
    }

    $center_width = $width / 1; //按宽度缩放
    $center_height = $height / 1; //按高度缩放
    $scale = $src_width / $center_width; //缩略图的宽度缩放比(本身宽度/组合后的宽度) 按宽度缩放
    $scale2 = $src_height / $center_height; //缩略图的宽度缩放比(本身高度/组合后的高度) 按高度缩放

    if ($center_width < $src_width) {
        $center_height = $src_height / $scale; //组合之后缩略图的高度
        $center_width = $src_width / $scale; //组合之后缩略图的高度
    } else {
        $center_height = $src_height / $scale2; //组合之后缩略图的高度
        $center_width = $src_width / $scale2; //组合之后缩略图的高度
    }

    $from_width = ($width - $center_width) / 2; //组合之后缩略图左上角所在坐标点
    $from_height = ($height - $center_height) / 2;

    //生成等比例的缩略图
    $tmpImage = imagecreatetruecolor($width, $height); //生成画布
    $color = imagecolorallocatealpha($tmpImage, 255, 255, 255, 127);
    imagefill($tmpImage, 0, 0, $color);
    imagealphablending($tmpImage, false); //不合并颜色,直接用$img图像颜色替换,包括透明色;
    imagesavealpha($tmpImage, true); //不要丢了$thumb图像的透明色;
    imagecopyresampled($tmpImage, $src_image, $from_width, $from_height, 0, 0, $center_width, $center_height, $src_width, $src_height);
    //生成缩略图图片
    imagepng($tmpImage, $thumb_file);
    imagedestroy($tmpImage);
    if (is_file($thumb_file)) {
        $thumb_file = str_replace(WEBPATH, '/', $thumb_file);
        return SITE_URL . trim($thumb_file, '/');
    } else {
        return $default;
    }
}

function ams_mkdirs($dir, $null = true)
{
    if (!$dir) {
        return false;
    }
    if (!is_dir($dir)) {
        ams_mkdirs(dirname($dir));
        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }
    }
}

function ams_safe_replace($string, $diy = null)
{
    $replace = ['%20', '%27', '%2527', '*', "'", '"', ';', '<', '>', "{", '}'];
    if (isset($diy) && $diy) {
        if (is_array($diy)) {
            $replace = ams_array2array($replace, $diy);
        } else {
            $replace[] = $diy;
        }
    }
    return str_replace($replace, '', $string);
}

function ams_safe_filename($string = '')
{
    return str_replace(
        ['..', "/", '\\', ' ', '<', '>', "{", '}', ';', '[', ']', '\'', '"', '*', '?'],
        '',
        $string
    );
}

function ams_safe_username($string = '')
{
    return str_replace(
        ['..', "/", '\\', ' ', "#", '\'', '"'],
        '',
        $string
    );
}

function ams_safe_password($string = '')
{
    return trim(str_replace(["'", '"', '&', '?'], '', $string));
}

function ams_pieces($code)
{
    ams_clean_xss($code);
    if (!$code) {
        return null;
    }
    $piecesModel = model('App\Models\PiecesModel');
    $data = $piecesModel->getByName($code);
    return $data;
}

/**
 * 格式化搜索关键词参数
 */
function ams_meta_keyword($str = '')
{
    if ($str) {
        $str = str_replace(array('，', '、', '；', ';', ' ', PHP_EOL, "\r", "\n", "\r\n"), ',', $str);
    }
    return $str;
}

/**
 * 清除HTML标记
 * @param string $str
 * @return  string
 */
function ams_clearhtml($str)
{

    $str = str_replace(
        ['&nbsp;', '&amp;', '&quot;', '&#039;', '&ldquo;', '&rdquo;', '&mdash;', '&lt;', '&gt;', '&moddot;', '&hellip;'],
        [' ', '&', '"', "'", '“', '”', '—', '<', '>', '·', '…'],
        $str
    );

    $str = preg_replace("/\<[a-z]+(.*)\>/iU", "", $str);
    $str = preg_replace("/\<\/[a-z]+\>/iU", "", $str);
    $str = preg_replace("/{.+}/U", "", $str);
    $str = str_replace([chr(13), chr(10), '&nbsp;'], '', $str);
    $str = strip_tags($str);

    return trim($str);
}

/**
 * 字符截取 支持UTF8/GBK
 * @param $string
 * @param $length
 * @param $dot
 */
function ams_strcut($string, $length, $dot = '...')
{
    $strlen = strlen($string);
    if ($strlen <= $length) {
        return $string;
    }

    $string = str_replace(array('&nbsp;', '&amp;', '&quot;', '&#039;', '&ldquo;', '&rdquo;', '&mdash;', '&lt;', '&gt;', '&middot;', '&hellip;'), array(' ', '&', '"', "'", '“', '”', '—', '<', '>', '·', '…'), $string);
    $strcut = '';
    if (strtolower(CHARSET) == 'utf-8') {
        $n = $tn = $noc = 0;
        while ($n < $strlen) {
            $t = ord($string[$n]);
            if ($t == 9 || $t == 10 || (32 <= $t && $t <= 126)) {
                $tn = 1;
                $n++;
                $noc++;
            } elseif (194 <= $t && $t <= 223) {
                $tn = 2;
                $n += 2;
                $noc += 2;
            } elseif (224 <= $t && $t < 239) {
                $tn = 3;
                $n += 3;
                $noc += 2;
            } elseif (240 <= $t && $t <= 247) {
                $tn = 4;
                $n += 4;
                $noc += 2;
            } elseif (248 <= $t && $t <= 251) {
                $tn = 5;
                $n += 5;
                $noc += 2;
            } elseif ($t == 252 || $t == 253) {
                $tn = 6;
                $n += 6;
                $noc += 2;
            } else {
                $n++;
            }
            if ($noc >= $length) {
                break;
            }

        }
        if ($noc > $length) {
            $n -= $tn;
        }

        $strcut = substr($string, 0, $n);
    } else {
        $dotlen = strlen($dot);
        $maxi = $length - $dotlen - 1;
        for ($i = 0; $i < $maxi; $i++) {
            $strcut .= ord($string[$i]) > 127 ? $string[$i] . $string[++$i] : $string[$i];
        }
    }
    $strcut = str_replace(array('&', '"', "'", '<', '>'), array('&amp;', '&quot;', '&#039;', '&lt;', '&gt;'), $strcut);
    return $strcut . $dot;
}
