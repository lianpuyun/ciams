<?php

namespace App\Models;

use CodeIgniter\Model;

class ComModel extends Model
{
    public $prefix;

    protected $table;
    protected $db;

    public function __construct(...$param)
    {
        parent::__construct();
        $this->db = \Config\Database::connect('default');
        $this->prefix = $this->db->DBPrefix;
    }

    //检查 表tablename 字段field 值value 某个id 是否存在
    protected function field_exitsts($tablename, $field, $value, $id = 0, $idcell = 'id')
    {
        if (!$tablename || !$field || !$value) {
            return true;
        }
        $rs = $this->db->table($tablename)->where($field, $value)->where($idcell . '<>', $id)->countAllResults();
        return $rs;
    }

    //更新排序
    protected function displayorder($tablename, $id, $displayorder = 0)
    {
        $displayorder = intval($displayorder);
        if (!$id || !$tablename) {
            return ams_rt(1, '参数不全');
        }
        $rs = $this->db->table($tablename)->where('id', $id)->update(['displayorder' => $displayorder]);
        if ($rs) {
            return ams_rt(0, '操作成功');
        }
        $msg = $this->db->error();
        return ams_rt(1, '操作失败:' . ($msg['message'] ?? ''));
    }

    //附件归属
    protected function attachments($id, $related)
    {
        if ($related && $id) {
            $this->db->table('attachment')->where('id', intval($id))->update(['related' => $related]);
        }
    }
}
