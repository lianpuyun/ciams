<?php

namespace App\Libraries\Ext;

use \CodeIgniter\View\Exceptions\ViewException;

class MyView extends \CodeIgniter\View\View

{

    public $rootView;

    /**
     * Builds the output based upon a file name and any
     * data that has already been set.
     *
     * Valid $options:
     *     - cache         number of seconds to cache for
     *  - cache_name    Name to use for cache
     *
     * @param string  $view
     * @param array   $options
     * @param boolean $saveData
     *
     * @return string
     */
    public function render(string $view, array $options = null, bool $saveData = null): string
    {
        $this->renderVars['start'] = microtime(true);

        // Store the results here so even if
        // multiple views are called in a view, it won't
        // clean it unless we mean it to.
        if (is_null($saveData)) {
            $saveData = $this->saveData;
        }

        //简单处理下
        $view = str_replace('..', '', $view);
        $first = substr($view, 0, 1);
        if ($first === '/') {
            $view = ltrim($view, '/');
        }

        $fileExt = pathinfo($view, PATHINFO_EXTENSION);
        $realPath = empty($fileExt) ? $view . '.php' : $view; // allow Views as .html, .tpl, etc (from CI3)
        $this->renderVars['view'] = $realPath;
        $this->renderVars['options'] = $options;

        //区分目录
        $viewDir = '';
        if (defined('IS_ADMIN') && IS_ADMIN) {
            //后台视图目录
            $viewDir = 'admin/';
        } else if (defined('IS_INSTALL') && IS_INSTALL) {
            //安装视图目录
            $viewDir = 'install/';
        } else {
            $request = \Config\Services::request();
            $agent = $request->getUserAgent();
            //前端模板视图目录
            $viewDir = 'public/';
            if ((defined('SITE_MOBILE_OPEN') && SITE_MOBILE_OPEN) || (defined('SITE_MOBILE') && SITE_MOBILE) && $agent->isMobile()) {
                $viewDir = $viewDir . 'mobile/';
            } else {
                $viewDir = $viewDir . 'pc/';
            }
        }

        $rootView = APPPATH . 'Views/'; //主项目视图
        $modView = ''; //模块视图Views

        //模块视图
        if (defined('MODDIR') && MODDIR) {
            $modView = APPSPATH . MODDIR . '/Views/';
            if ($first != '/') {
                if (is_file($modView . $viewDir . $this->renderVars['view'])) {
                    $rootView = $modView;
                }
            }
        }

        // Was it cached?
        if (isset($this->renderVars['options']['cache'])) {
            $this->renderVars['cacheName'] = $this->renderVars['options']['cache_name'] ?? str_replace('.php', '', $this->renderVars['view']);
            if ($output = cache($this->renderVars['cacheName'])) {
                $this->logPerformance($this->renderVars['start'], microtime(true), $this->renderVars['view']);
                return $output;
            }
        }

        $this->viewPath = $rootView;

        $this->renderVars['file'] = $this->viewPath . $viewDir . $this->renderVars['view'];

        if (!is_file($this->renderVars['file'])) {
            $this->renderVars['file'] = $this->loader->locateFile($this->renderVars['view'], 'Views', empty($fileExt) ? 'php' : $fileExt);
        }

        // locateFile will return an empty string if the file cannot be found.
        if (empty($this->renderVars['file'])) {
            throw new \Exception('Template File (' . $viewDir . $this->renderVars['view'] . ') Is Not Find!', '404');
            throw ViewException::forInvalidFile($this->renderVars['view']);
        }

        // Make our view data available to the view.

        if (is_null($this->tempData)) {
            $this->tempData = $this->data;
        }

        extract($this->tempData);

        if ($saveData) {
            $this->data = $this->tempData;
        }

        ob_start();
        include $this->renderVars['file']; // PHP will be processed
        $output = ob_get_contents();
        @ob_end_clean();

        // When using layouts, the data has already been stored
        // in $this->sections, and no other valid output
        // is allowed in $output so we'll overwrite it.
        if (!is_null($this->layout) && empty($this->currentSection)) {
            $layoutView = $this->layout;
            $this->layout = null;
            $output = $this->render($layoutView, $options, $saveData);
        }

        $this->logPerformance($this->renderVars['start'], microtime(true), $this->renderVars['view']);

        if ($this->debug && (!isset($options['debug']) || $options['debug'] === true)) {
            $toolbarCollectors = config(\Config\Toolbar::class)->collectors;

            if (in_array(\CodeIgniter\Debug\Toolbar\Collectors\Views::class, $toolbarCollectors)) {
                // Clean up our path names to make them a little cleaner
                foreach (['APPPATH', 'SYSTEMPATH', 'ROOTPATH'] as $path) {
                    if (strpos($this->renderVars['file'], constant($path)) === 0) {
                        $this->renderVars['file'] = str_replace(constant($path), $path . '/', $this->renderVars['file']);
                        break;
                    }
                }
                $this->renderVars['file'] = ++$this->viewsCount . ' ' . $this->renderVars['file'];
                $output = '<!-- DEBUG-VIEW START ' . $this->renderVars['file'] . ' -->' . PHP_EOL
                . $output . PHP_EOL
                . '<!-- DEBUG-VIEW ENDED ' . $this->renderVars['file'] . ' -->' . PHP_EOL;
            }
        }

        // Should we cache?
        if (isset($this->renderVars['options']['cache'])) {
            cache()->save($this->renderVars['cacheName'], $output, (int) $this->renderVars['options']['cache']);
        }

        $this->tempData = null;

        return $output;
    }

}
