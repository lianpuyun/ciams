<?php
namespace App\Libraries\Ext;

use \CodeIgniter\Exceptions\PageNotFoundException;
use \CodeIgniter\HTTP\Request;
use \CodeIgniter\Router\Exceptions\RedirectException;
use \CodeIgniter\Router\Exceptions\RouterException;

class MyRouter extends \CodeIgniter\Router\Router

{

    protected $appsController; // apps 控制器文件
    protected $appsDirectory; //apps controllers 子目录
    protected $appsDir; //apps 目录


    public function __construct(\CodeIgniter\Router\RouteCollectionInterface $routes, ?Request $request = null)
    {
        parent::__construct($routes,$request);
        // your code here

        
    }

    /**
     * Attempts to match a URI path against Controllers and directories
     * found in APPPATH/Controllers, to find a matching route.
     *
     * @param string $uri
     */
    public function autoRoute(string $uri)
    {
        $segments = explode('/', $uri);

        // If not empty, then the first segment should be the controller
        if (isset($segments[0]) && $segments[0] && stristr($segments[0], '.php')) {
            array_shift($segments);
        }

        //main app first
        $segments = $this->scanControllers($segments);
        $file = null;

        // If we don't have any segments left - try the default controller;
        // WARNING: Directories get shifted out of the segments array.

        if (empty($segments) || !$segments) {

            $this->setDefaultController();
            $controllerName = $this->controllerName();

            if (!$this->isValidSegment($controllerName)) {
                throw new PageNotFoundException($this->controller . ' is not a valid controller name');
            }

        } else {

            // If direcrory, then the first segment should be the controller
            if ($this->directory) {

                $this->controller = ucfirst(array_shift($segments));
                $controllerName = $controllerName = $this->controllerName();
                if (!$this->isValidSegment($controllerName)) {
                    throw new PageNotFoundException($this->controller . ' is not a valid controller name');
                }

                if (!empty($segments)) {
                    $this->methodName = $this->method = array_shift($segments);
                }

            } else {
                // If no direcrory, then the first segment should be the main controller or apps dir

                $whichName = ucfirst(array_shift($segments));

                if (!$this->isValidSegment($whichName)) {

                    throw new PageNotFoundException($whichName . ' is not a valid controller name');
                }

                if (is_file(APPPATH . 'Controllers/' . ucfirst($whichName) . '.php')) {
                    $this->controller = $whichName;
                    $controllerName = $this->controllerName();

                    if (!$this->isValidSegment($controllerName)) {
                        throw new PageNotFoundException($this->controller . ' is not a valid controller name');
                    }

                    if (!empty($segments)) {
                        $this->methodName = $this->method = array_shift($segments);
                    }

                } elseif (is_dir(APPSPATH . ucfirst($whichName) . '/')) {
                    //apps-dir-controllers
                    $this->appsDir = $whichName;
                    $appsDirName = $whichName;

                    define('MODDIR', ucfirst($appsDirName));

                    $this->collection->setAppsDefaultNamespace('Apps\\' . ucfirst($appsDirName) . '\Controllers');

                    $segments = $this->scanAppsControllers($segments);

                    if ($this->appsDirectory) {

                        $this->controller = ucfirst(array_shift($segments));
                        $controllerName = $this->controllerName();
                        if (!$this->isValidSegment($controllerName)) {

                            throw new PageNotFoundException($this->controller . ' is not a valid controller name');
                        }

                        if (!empty($segments)) {
                            $this->methodName = $this->method = array_shift($segments);
                        }

                    } else {

                        if (!empty($segments)) {
                            $this->controller = ucfirst(array_shift($segments));
                        }

                        $controllerName = $this->controllerName();
                        if (!$this->isValidSegment($controllerName)) {
                            throw new PageNotFoundException($this->controller . ' is not a valid controller name');
                        }
                        
                        if (!empty($segments)) {
                            $this->method = array_shift($segments) ?: $this->method;
                        }

                    }

                } else {

                    throw new PageNotFoundException();
                }

            }

        }
        
        //default main app
        if (!defined('MODDIR')) {
            define('MODDIR', '');
        }

        if (!empty($segments)) {
            $this->params = $segments;
        }

        $defaultNamespace = $this->collection->getDefaultNamespace();

        if ($this->collection->getHTTPVerb() !== 'cli') {
            $controller = '\\' . $defaultNamespace;

            $controller .= $this->directory ? str_replace('/', '\\', $this->directory) : '';
            $controller .= $controllerName;

            $controller = strtolower($controller);
            $methodName = strtolower($this->methodName());

            foreach ($this->collection->getRoutes('cli') as $route) {

                if (is_string($route)) {
                    $route = strtolower($route);

                    if (strpos($route, $controller . '::' . $methodName) === 0) {
                        throw new PageNotFoundException();
                    }

                    if ($route === $controller) {
                        throw new PageNotFoundException();
                    }
                }
            }
        }

        if ($this->appsDirectory) {
            $file = APPSPATH . MODDIR . '/Controllers/' . $this->appsDirectory . $controllerName . '.php';
        } else {
            $file = APPPATH . 'Controllers/' . $this->directory . $controllerName . '.php';
        }

        // Load the file so that it's available for CodeIgniter.
        if (is_file($file)) {
            include_once $file;
        }

        // Ensure the controller stores the fully-qualified class name
        // We have to check for a length over 1, since by default it will be '\'
        if (strpos($this->controller, '\\') === false && strlen($this->collection->getDefaultNamespace()) > 1) {
            $this->controller = '\\' . ltrim(str_replace('/', '\\', $this->collection->getDefaultNamespace() . ($this->appsDirectory ? $this->appsDirectory : $this->directory) . $this->controllerName()), '\\');
        }

    }

    /**
     * Scans the controller directory, attempting to locate a controller matching the supplied uri $segments
     *
     * @param array $segments URI segments
     *
     * @return array returns an array of remaining uri segments that don't map onto a directory
     *
     * @deprecated this function name does not properly describe its behavior so it has been deprecated
     *
     * @codeCoverageIgnore
     */
    protected function validateRequest(array $segments): array
    {
        return $this->scanControllers($segments);
    }

    /**
     * Scans the controller directory, attempting to locate a controller matching the supplied uri $segments
     *
     * @param array $segments URI segments
     *
     * @return array returns an array of remaining uri segments that don't map onto a directory
     */
    protected function scanControllers(array $segments): array
    {
        $segments = array_filter($segments, function ($segment) {
            return $segment !== '';
        });
        // numerically reindex the array, removing gaps
        $segments = array_values($segments);

        // if a prior directory value has been set, just return segments and get out of here
        if (isset($this->directory)) {
            return $segments;
        }

        // Loop through our segments and return as soon as a controller
        // is found or when such a directory doesn't exist
        $c = count($segments);

        while ($c-- > 0) {
            $segmentConvert = ucfirst($this->translateURIDashes === true ? str_replace('-', '_', $segments[0]) : $segments[0]);
            // as soon as we encounter any segment that is not PSR-4 compliant, stop searching

            if (!$this->isValidSegment($segmentConvert)) {
                return $segments;
            }

            $test = APPPATH . 'Controllers/' . $this->directory . $segmentConvert;

            // as long as each segment is *not* a controller file but does match a directory, add it to $this->directory
            if (!is_file($test . '.php') && is_dir($test)) {
                $this->setDirectory($segmentConvert, true, false);
                array_shift($segments);
                continue;
            }

            return $segments;
        }

        // This means that all segments were actually directories
        return $segments;
    }

    private function isValidSegment(string $segment): bool
    {
        return (bool) preg_match('/^[a-zA-Z_\x80-\xff][a-zA-Z0-9_\x80-\xff]*$/', $segment);
    }


    /**
     * Sets the sub-directory that the controller is in.
     *
     * @param bool $validate if true, checks to make sure $dir consists of only PSR4 compliant segments
     *
     * @deprecated This method should be removed.
     */
    public function setDirectory(?string $dir = null, bool $append = false, bool $validate = true)
    {
        if (empty($dir)) {
            $this->directory = null;

            return;
        }

        if ($validate) {
            $segments = explode('/', trim($dir, '/'));

            foreach ($segments as $segment) {
                if (! $this->isValidSegment($segment)) {
                    return;
                }
            }
        }

        if ($append !== true || empty($this->directory)) {
            $this->directory = trim($dir, '/') . '/';
        } else {
            $this->directory .= trim($dir, '/') . '/';
        }
    }

    /**
     * Sets the default controller based on the info set in the RouteCollection.
     */
    protected function setAppsDefaultController()
    {
        if (empty($this->controller)) {
            throw RouterException::forMissingDefaultRoute();
        }

        // Is the method being specified?
        if (sscanf($this->controller, '%[^/]/%s', $class, $this->method) !== 2) {
            $this->method = 'index';
        }

        if (!is_file(APPSPATH . 'Controllers/' . $this->directory . ucfirst($class) . '.php')) {
            return;
        }

        $this->controller = ucfirst($class);

        log_message('info', 'Used the default controller.');
    }

    /**
     * Scans the controller appsDirectory, attempting to locate a controller matching the supplied uri $segments
     *
     * @param array $segments URI segments
     *
     * @return array returns an array of remaining uri segments that don't map onto a appsDirectory
     */
    protected function scanAppsControllers(array $segments): array
    {
        $segments = array_filter($segments, function ($segment) {
            return $segment !== '';
        });
        // numerically reindex the array, removing gaps
        $segments = array_values($segments);

        // if a prior appsDirectory value has been set, just return segments and get out of here
        if (isset($this->appsDirectory)) {
            return $segments;
        }

        // Loop through our segments and return as soon as a controller
        // is found or when such a appsDirectory doesn't exist
        $c = count($segments);

        while ($c-- > 0) {
            $segmentConvert = ucfirst($this->translateURIDashes === true ? str_replace('-', '_', $segments[0]) : $segments[0]);
            // as soon as we encounter any segment that is not PSR-4 compliant, stop searching

            if (!$this->isValidSegment($segmentConvert)) {
                return $segments;
            }

            $test = APPSPATH . MODDIR . '/Controllers/' . $this->appsDirectory . $segmentConvert;

            // as long as each segment is *not* a controller file but does match a appsDirectory, add it to $this->appsDirectory
            if (!is_file($test . '.php') && is_dir($test)) {
                $this->setAppsDirectory($segmentConvert, true, false);
                array_shift($segments);
                continue;
            }

            return $segments;
        }

        // This means that all segments were actually directories
        return $segments;
    }

    /**
     * Sets the sub-directory that the controller is in.
     *
     * @param string|null $dir
     * @param boolean     $append
     * @param boolean     $validate if true, checks to make sure $dir consists of only PSR4 compliant segments
     */
    public function setAppsDirectory(string $dir = null, bool $append = false, bool $validate = true)
    {
        if (empty($dir)) {
            $this->appsDirectory = null;
            return;
        }

        if ($validate) {
            $segments = explode('/', trim($dir, '/'));
            foreach ($segments as $segment) {
                if (!$this->isValidSegment($segment)) {
                    return;
                }
            }
        }

        if ($append !== true || empty($this->appsDirectory)) {
            $this->appsDirectory = trim($dir, '/') . '/';
        } else {
            $this->appsDirectory .= trim($dir, '/') . '/';
        }
    }

}
