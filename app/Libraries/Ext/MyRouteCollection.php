<?php
namespace App\Libraries\Ext;

use CodeIgniter\HTTP\RequestInterface;
use \CodeIgniter\Router\RouteCollectionInterface;

class MyRouteCollection extends \CodeIgniter\Router\RouteCollection
{

    // public $isapps = 0;
    protected $isapps = false;

    /**
     * Sets the default namespace to use for Controllers when no other
     * namespace has been specified.
     *
     * @param $value
     *
     * @return \CodeIgniter\Router\RouteCollectionInterface
     */
    public function setDefaultNamespace(string $value): RouteCollectionInterface
    {
        $this->defaultNamespace = filter_var($value, FILTER_SANITIZE_STRING);
        $this->defaultNamespace = rtrim($this->defaultNamespace, '\\') . '\\';
        $this->isapps = true;
        return $this;
    }

    /**
     * If TRUE, the system will attempt to match the URI against
     * Controllers by matching each segment against folders/files
     * in APPPATH/Controllers, when a match wasn't found against
     * defined routes.
     *
     * If FALSE, will stop searching and do NO automatic routing.
     *
     * @param boolean $value
     *
     * @return RouteCollectionInterface
     */
    public function setAutoRoute(bool $value): RouteCollectionInterface
    {
        $this->autoRoute = $value;

        return $this;
    }

    public function setAppsDefaultNamespace(string $value): RouteCollectionInterface
    {
        $this->setDefaultNamespace($value);
        return $this;
    }

    public function isApps($request = null)
    {
        $segments = $request->uri->getSegments();
        $this->setDefaultNamespace('App\Controllers');
    }

}
