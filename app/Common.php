<?php

/**
 * The goal of this file is to allow developers a location
 * where they can overwrite core procedural functions and
 * replace them with their own. This file is loaded during
 * the bootstrap process and is called during the frameworks
 * execution.
 *
 * This can be looked at as a `master helper` file that is
 * loaded early on, and may also contain additional functions
 * that you'd like to use throughout your entire application
 *
 * @link: https://codeigniter4.github.io/CodeIgniter4/
 */

//应用目录
function get_module_dirs()
{
    $source_dir = APPSPATH;
    if ($fp = @opendir($source_dir)) {
        $filedata = array();
        $source_dir = rtrim($source_dir, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;
        while (false !== ($file = readdir($fp))) {
            if ($file === '.' or $file === '..' or ($file[0] === '.') or !@is_dir($source_dir . $file)) {
                continue;
            }
            $filedata[] = $file;
        }
        closedir($fp);
        return $filedata;
    }
    return false;
}
