<?php
// Path to the front controller (this file)
define('FCPATH', __DIR__ . DIRECTORY_SEPARATOR);

// Ensure the current directory is pointing to the front controller's directory
chdir(FCPATH);

//==============================start
!defined('SELF') && define('SELF', pathinfo(__FILE__, PATHINFO_BASENAME));

// Path to the front controller (this file)
define('FCPATH', dirname(__DIR__) . DIRECTORY_SEPARATOR);
define('WEBPATH', __DIR__ . DIRECTORY_SEPARATOR);
define('APPSPATH', FCPATH . 'apps' . DIRECTORY_SEPARATOR);

//打开调试器1/关闭0
define('CI_DEBUG', 1);

// Path to the front controller (this file)

if (defined('STDIN') || PHP_SAPI === 'cli') {
    
    define('CIAMS_BASE_URL', 'http://127.0.0.1/');
    define('MOK_NOW_URL', 'http://127.0.0.1/');
    define('DOMAIN_NAME', 'http://127.0.0.1/');

} else {

    //当前URL
    $pageURL = 'http';
    if ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == '443')) {
        $pageURL .= 's';
    }
    $pageURL .= '://';
    if (strpos($_SERVER['HTTP_HOST'], ':') !== false) {
        $url = explode(':', $_SERVER['HTTP_HOST']);
        $url[0] ? $pageURL .= $_SERVER['HTTP_HOST'] : $pageURL .= $url[0];
    } else {
        $pageURL .= $_SERVER['HTTP_HOST'];
    }

    //当前域名
    define('CIAMS_BASE_URL', $pageURL);
    $pageURL .= $_SERVER['REQUEST_URI'] ? $_SERVER['REQUEST_URI'] : $_SERVER['PHP_SELF'];
    //当前url
    define('MOK_NOW_URL', $pageURL);
    // 当前域名
    define('DOMAIN_NAME', strtolower($_SERVER['HTTP_HOST']));

}

//===============================end

/*
 *---------------------------------------------------------------
 * BOOTSTRAP THE APPLICATION
 *---------------------------------------------------------------
 * This process sets up the path constants, loads and registers
 * our autoloader, along with Composer's, loads our constants
 * and fires up an environment-specific bootstrapping.
 */

// Load our paths config file
// This is the line that might need to be changed, depending on your folder structure.
require FCPATH . '../app/Config/Paths.php';
// ^^^ Change this line if you move your application folder

$paths = new Config\Paths();

// Location of the framework bootstrap file.
require rtrim($paths->systemDirectory, '\\/ ') . DIRECTORY_SEPARATOR . 'bootstrap.php';

// Load environment settings from .env files into $_SERVER and $_ENV
require_once SYSTEMPATH . 'Config/DotEnv.php';
(new CodeIgniter\Config\DotEnv(ROOTPATH))->load();

/*
 * ---------------------------------------------------------------
 * GRAB OUR CODEIGNITER INSTANCE
 * ---------------------------------------------------------------
 *
 * The CodeIgniter class contains the core functionality to make
 * the application run, and does all of the dirty work to get
 * the pieces all working together.
 */

$app = Config\Services::codeigniter();
$app->initialize();
$context = is_cli() ? 'php-cli' : 'web';
$app->setContext($context);

/*
 *---------------------------------------------------------------
 * LAUNCH THE APPLICATION
 *---------------------------------------------------------------
 * Now that everything is setup, it's time to actually fire
 * up the engines and make this app do its thang.
 */

$app->run();
