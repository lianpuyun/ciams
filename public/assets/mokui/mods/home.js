layui.define(['element', 'layer', 'tabs'], function (exports) {

    var $ = layui.$,
        $body = $('body'),
        element = layui.element,
        layer = layui.layer,
        tabs = layui.tabs;

    var Home = {
        openTabs: function (options) {
            tabs.add(options.title, options.url);
        },
        ajax: function (url, type, dataType, data, callback) {
            $.ajax({
                url: url,
                type: type,
                dataType: dataType,
                data: data,
                success: callback,
                error: function () {
                    layer.msg('服务器错误', { icon: 5, time: 2000 });
                },
                complete: function () {
                    console.log('ajax complate');
                },
            });
        }
    }

    exports('home', Home);
});