layui.define(['element', 'layer'], function (exports) {

    var $ = layui.$,
        $body = $('body'),
        element = layui.element,
        layer = layui.layer;

    var screen_size = {
        pc: [991, -1],
        pad: [768, 990],
        mobile: [0, 767]
    }

    var getDevice = function () {
        var width = $(window).width();
        for (var i in screen_size) {
            var sizes = screen_size[i],
                min = sizes[0],
                max = sizes[1];
            if (max == -1) max = width;
            if (min <= width && max >= width) {
                return i;
            }
        }
        return null;
    }

    var isDevice = function (label) {
        return getDevice() == label;
    }

    var isMobile = function () {
        return isDevice('mobile');
    }

    var Tab = function (el) {
        if (el == 'undefined' || !el) {
            el = 'tabs';
        }
        this.el = el;
        this.urls = [];
    }

    Tab.fn = Tab.prototype;

    Tab.fn.content = function (src) {
        var iframe = document.createElement("iframe");
        iframe.setAttribute("frameborder", "0");
        iframe.setAttribute("src", src);
        iframe.setAttribute("data-id", this.urls.length);
        return iframe.outerHTML;
    };

    Tab.fn.init = function () {
        var $this = $("#Nav li:eq(0)");
        $this.find('dl.layui-nav-child > dd > a:eq(0)').trigger('click');
        $this.addClass('layui-nav-itemed');
    }

    Tab.fn.is = function (url) {
        return (this.urls.indexOf(url) !== -1)
    };

    Tab.fn.add = function (title, url) {
        if (tabs.is(url)) {
            tabs.change(url);
            return false;
        }
        tabs.urls.push(url);
        element.tabAdd(this.el, {
            title: title,
            content: this.content(url),
            id: url
        });
        tabs.change(url);
    };

    Tab.fn.change = function (url) {
        element.tabChange(this.el, url);
    };

    Tab.fn.delete = function (url) {
        element.tabDelete(this.el, url);
    };

    Tab.fn.deleteall = function (urls) {
        $.each(urls, function (i, item) {
            element.tabDelete(this.el, item);
        })
    }

    Tab.fn.onChange = function (callback) {
        element.on('tab(' + this.el + ')', callback);
    };

    Tab.fn.onDelete = function (callback) {
        var self = this;
        element.on('tabDelete(' + this.el + ')', function (data) {
            var i = data.index;
            self.urls.splice(i, 1);
            callback && callback(data);
        });
    };

    //tab标签
    function rollPage(direction) {
        var $tabTitle = $('#appTabs');
        var left = $tabTitle.scrollLeft();
        if ('left' === direction) {
            $tabTitle.animate({
                scrollLeft: left - 120
            }, 300);
        } else if ('right' === direction) {
            $tabTitle.animate({
                scrollLeft: left + 120
            }, 300);
        }
    }

    function slideSideBar() {

        var $slideSidebar = $('.slide-sidebar'),
            $pageContainer = $('.layui-body'),
            $mobileMask = $('.mobile-mask');

        var isFold = false;

        if (isMobile()) {
            $(".slide-sidebar i").removeClass('layui-icon-spread-right').addClass('layui-icon-spread-left');
            $admin = $body.find('.layui-layout-admin');
            $admin.addClass('fold-side-bar');
            isFold = true;
        }

        $slideSidebar.click(function (e) {
            e.preventDefault();
            var $this = $(this),
                $icon = $this.find('i'),
                $admin = $body.find('.layui-layout-admin');
            var toggleClass = isMobile() ? 'fold-side-bar' : 'fold-side-bar';//fold-side-bar-xs
            console.log(toggleClass);
            if ($icon.hasClass('layui-icon-spread-right')) {
                $icon.removeClass('layui-icon-spread-right').addClass('layui-icon-spread-left');
                $admin.addClass(toggleClass);
                isFold = true;
                // if (isMobile()) $mobileMask.show();
            } else {
                $icon.removeClass('layui-icon-spread-left').addClass('layui-icon-spread-right');
                $admin.removeClass(toggleClass);
                isFold = false;
                if (isMobile()) $mobileMask.hide();
            }
        });

        var tipIndex;
        // 菜单收起后的模块信息小提示
        $('#Nav li > a').hover(function () {
            var $this = $(this);
            if (isFold) {
                tipIndex = layer.tips($this.find('em').text(), $this);
            }
        }, function () {
            if (isFold && tipIndex) {
                layer.close(tipIndex);
                tipIndex = null
            }
        })

        //菜单收起后点击图标
        $('#Nav li > a').click(function () {
            if (isFold) {

            }
        });

        if (isMobile()) {
            $mobileMask.click(function () {
                $slideSidebar.trigger('click');
            });
        }
    }

    var tabs = new Tab('tabs'), navItems = [];

    $('#Nav a').on('click', function (event) {
        event.preventDefault();
        var $this = $(this),
            url = $this.attr('href'),
            title = $.trim($this.text());
        if (url && url !== 'javascript:;') {
            if (tabs.is(url)) {
                tabs.change(url);
            } else {
                navItems.push($this);
                tabs.add(title, url);
            }
            //顶部菜单高亮
        }
        // $this.closest('li.layui-nav-item').addClass('layui-nav-itemed').siblings().removeClass('layui-nav-itemed');
    });

    tabs.onChange(function (data) {
        var i = data.index,
            $this = navItems[i];
        if ($this && typeof $this === 'object') {
            $('#Nav dd').removeClass('layui-this');
            $this.parent('dd').addClass('layui-this');
            $this.closest('li.layui-nav-item').addClass('layui-nav-itemed').siblings().removeClass('layui-nav-itemed');
        }
    });

    tabs.onDelete(function (data) {
        var i = data.index;
        navItems.splice(i, 1);
    });

    slideSideBar();


    //右侧tab控制
    //刷新当前页
    $("#freshThis").on('click', function () {
        var layId = $("#appTabs").find("li.layui-this").attr('lay-id');
        if (layId) {
            tabs.onChange(layId);
            var src = $("#appTabPage").children(".layui-show").find('iframe').attr("src");
            $("#appTabPage").children(".layui-show").find('iframe').attr("src", src);
        }
    });

    //关闭当前标签页
    $("#closeThis").on('click', function () {
        var layId = $("#appTabs").find("li.layui-this").attr('lay-id');
        if (layId) {
            tabs.delete(layId);
        }
    });

    //关闭其它标签页
    $("#closeOther").on('click', function () {
        var thisId = $("#appTabs").find("li.layui-this").attr('lay-id');
        if (thisId) {
            $('#appTabs').find('li').each(function (i, o) {
                var layId = $(o).attr('lay-id');
                if (layId != thisId && layId != 0 && layId != '') {
                    tabs.delete(layId);
                }
            });
        }
    });

    //关闭全部标签页
    $("#closeAll").on('click', function () {
        var tabtitle = $("#appTabs li");
        $.each(tabtitle, function (i) {
            tabs.delete($(this).attr("lay-id"));
        })
    });
    //tab左右移动
    $(".mokui-tab-prev").click(function () {
        rollPage("left");
    });
    $(".mokui-tab-next").click(function () {
        rollPage("right");
    });


    // 默认触发第一个子菜单的点击事件
    var tt = new Tab();
    tt.init(); //初始化
    exports('tabs', tt);
});