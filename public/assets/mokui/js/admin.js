layui.use(['jquery', 'layer', 'form'], function () {
    var $ = layui.jquery,
        layer = layui.layer,
        form = layui.form;

    window.getUrlParam = function (query, variable) {
        var vars = query.split("?");
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split("=");
            if (pair[0] == variable) { return pair[1]; }
        }
        return (false);
    }

    //隐藏字符
    window.hidestr = function (str, frontLen, endLen) {
        var len = str.length - frontLen - endLen;
        var xing = '';
        for (var i = 0; i < len; i++) {
            xing += '*';
        }
        return str.substring(0, frontLen) + xing + str.substring(str.length - endLen);
    }

    window.logout = function () {
        layer.confirm('确定退出吗？', { icon: 3, title: '提示' }, function (index) {
            $.post(adminpath + 'admin/login/out', {}, function (res) {
                layer.msg('退出成功', { icon: 1, time: 1000 }, function () {
                    window.location.reload();
                });
            }, 'json');
            layer.close(index);
        });
    }

    $(".editmy").on('click', function () {
        openUri('修改资料', adminpath + 'admin/user/my', '500px', '80%');
    })

    $(".logout").on('click', function () {
        logout();
    })

    window.mok_tips = function (code, msg, time) {
        if (!msg || msg == '' || msg == '&nbsp;') {
            return
        }
        if (code == 0) {
            icon = 1
        } else {
            icon = 5
        }
        if (!time) {
            time = 1
        }
        layer.msg(msg, {
            time: time * 1000,
            icon: icon,
        });
    }

    window.mok_topinyin = function (name, from, letter) {
        var val = $("#mok_" + from).val();
        if ($("#mok_" + name).val()) {
            return false
        }
        $.get("/opim/pinyin/" + val + "?rand=" + Math.random(), function (data) {
            $("#mok_" + name).val(data);
            if (letter) {
                $("#mok_letter").val(data.substr(0, 1))
            }
        })
    }


    window.mok_isemail = function (name) {
        var val = $("#mok_" + name).val();
        var reg = /^[-_A-Za-z0-9]+@([_A-Za-z0-9]+\.)+[A-Za-z0-9]{2,3}$/;
        if (reg.test(val)) {
            return false
        } else {
            return true
        }
    }

    window.mok_isurl = function (name) {
        var val = $("#mok_" + name).val();
        var reg = /http(s)?:\/\/([\w-]+\.)+[\w-]+(\/[\w- .\/?%&=]*)?/;
        var Exp = new RegExp(reg);
        if (Exp.test(val) == true) {
            return false
        } else {
            return true
        }
    }

    window.mok_isdomain = function (name) {
        var val = $("#mok_" + name).val();
        if (val.indexOf("/") > 0) {
            return true
        } else {
            return false
        }
    }

    window.mok_remove_file = function (name, id) {
        layer.confirm(lang["confirm"], {
            title: lang["tips"],
            btn: ['确定', '取消'],
            yes: function (index) {
                var fileid = $("#fileid_" + name + "_" + id).val();
                var value = $("#mok_" + name + "_del").val();
                $("#files_" + name + "_" + id).remove();
                $("#mok_" + name + "_del").val(value + "|" + fileid);
                layer.close(index);
            }
        });
    }

    window.mok_delete_file_js = function (name) {
        $("#mok_" + name).val("");
        $(".file_info_" + name).remove()
    }

    window.mok_delete_file = function (name) {
        layer.confirm(lang["confirm"], {
            title: lang["tips"],
            btn: ['确定', '取消'],
        }, function (index) {
            $("#mok_" + name).val("");
            $(".file_info_" + name).remove();
            layer.close(index);
        });
    }

    window.mok_delete_file2 = function (name) {
        layer.confirm('您确定要删除吗？', {
            btn: ['确定', '取消']
        }, function (index) {
            $("#fileid_" + name).val("");
            $("#mok_my_" + name + "_list").html("");
            layer.close(index);
        }, function () {

        });
    }

    window.mok_remove_ext = function (str) {
        var reg = /\.\w+$/;
        return str.replace(reg, "")
    }


    window.mok_del_attach = function (fid) {
        $.post(adminpath + 'admin/uploads/delfile', { id: fid }, function (datas) { }, 'json');
    }

    //删除图片
    $("body").on('click', '.mokui-upload-item-delete', function () {
        var that = this;
        var obj = $(that).parent('.mokui-upload-item');
        var id = obj.attr('data-fileid');

        layer.confirm('确定删除吗？', { icon: 3, title: '提示' }, function (index) {
            if (id) {
                //文件id 服务器上删除
                $.post(adminpath + 'admin/uploads/delfile', { id, id }, function (res) {
                    if (res.code == 0) {
                        obj.remove();
                    } else {
                        layer.msg(res.msg, { time: 2000, anim: 6, icon: 6 });
                    }
                }, 'json')
            } else {
                obj.remove();
            }
            layer.close(index);
        });
    });

    //type 1整数 2小数 1. 整数：/^([^0][0-9]+|0)$/ 2. 小数：/^(([^0][0-9]+|0)\.([0-9]{1,2}))$/
    window.checkRate = function (type, nubmer) {
        if (type == 1) {
            var re = /^([^0][0-9]+|0)$/;
            if (!re.test(nubmer)) {
                return false;
            }
            return true;
        } else if (type == 2) {
            var re = /^(([^0][0-9]+|0)\.([0-9]{1,2})$)|^([^0][0-9]+|0)$/;
            if (!re.test(nubmer)) {
                return false;
            }
            return true;
        }
        return false;
    }

    window.mok_to_key = function () {
        $.get(adminpath + 'admin/opi/syskey', { id: 1 }, function (res) {
            if (res.code == 0) {
                $("#mok_sitekey").val(res.data);
            } else {
                layer.msg(res.msg, { time: 2000, anim: 6, icon: 6 });
            }
        }, 'json');
    }
});